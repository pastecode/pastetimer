﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Domain.Entities;

namespace PasteTimer.usermodels
{
    /// <summary>
    /// 角色分组
    /// </summary>
    [Index(nameof(Name), IsUnique = true)]
    [Comment("角色分组")]
    public class GradeInfo : Entity<int>
    {
        /// <summary>
        /// 角色分组名称
        /// </summary>
        [MaxLength(16)]
        public string Name { get; set; }

        /// <summary>
        /// 分组描述
        /// </summary>
        [MaxLength(64)]
        public string Desc { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public bool IsEnable { get; set; } = true;

    }
}
