﻿using Microsoft.EntityFrameworkCore;
using Volo.Abp.Domain.Entities;

namespace PasteTimer.usermodels
{
    /// <summary>
    /// 角色绑定权限
    /// </summary>
    [Comment("角色绑定权限")]
    public class GradeRole : Entity<int>
    {
        /// <summary>
        /// 组别ID
        /// </summary>
        public int GradeId { get; set; }

        /// <summary>
        /// 权限ID
        /// </summary>
        public int RoleId { get; set; }
    }

}
