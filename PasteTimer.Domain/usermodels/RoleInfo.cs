﻿using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Domain.Entities;

namespace PasteTimer.usermodels
{
    /// <summary>
    /// 权限列表
    /// </summary>
    [Comment("权限列表")]
    public class RoleInfo : Entity<int>
    {

        /// <summary>
        /// 权限模块
        /// </summary>
        [MaxLength(16)]
        public string Model { get; set; }

        /// <summary>
        /// 权限项
        /// </summary>
        [MaxLength(16)]
        public string Name { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        [MaxLength(64)]
        public string Desc { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public bool IsEnable { get; set; } = true;

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; } = 100;
    }
}
