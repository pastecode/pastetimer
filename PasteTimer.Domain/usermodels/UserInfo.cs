﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Domain.Entities;

namespace PasteTimer.usermodels
{
    /// <summary>
    /// 用户信息
    /// </summary>
    [Comment("用户信息")]
    public class UserInfo : Entity<int>
    {
        /// <summary>
        /// 用户名
        /// </summary>
        [MaxLength(32)]
        public string UserName { get; set; }

        /// <summary>
        /// 创建日期
        /// </summary>
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        [MaxLength(64)]
        public string Desc { get; set; }

        /// <summary>
        /// 邮箱
        /// </summary>
        [MaxLength(64)]
        public string Email { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        [MaxLength(64)]
        public string PassWord { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public bool IsEnable { get; set; } = true;

        /// <summary>
        /// 哪个角色？
        /// </summary>
        [MaxLength(16)]
        public string Grade { get; set; }
    }
}
