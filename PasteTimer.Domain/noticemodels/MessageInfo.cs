﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Domain.Entities;

namespace PasteTimer.noticemodels
{
    /// <summary>
    /// 消息类型
    /// </summary>
    [Index(nameof(Code), IsUnique = true)]
    public class MessageInfo : Entity<int>
    {

        /// <summary>
        /// nodeoffline nodeexception taskfail taskexception
        /// </summary>
        [MaxLength(16)]
        [Comment("消息类型代码")]
        public string Code { get; set; }

        /// <summary>
        /// 介绍唯一key的计算方式
        /// </summary>
        [MaxLength(64)]
        [Comment("消息介绍")]
        public string Desc { get; set; }

        /// <summary>
        /// x(s) 发送频率大于1秒
        /// </summary>
        [Comment("消息发送最小频率")]
        public int SendRate { get; set; } = 300;

    }
}
