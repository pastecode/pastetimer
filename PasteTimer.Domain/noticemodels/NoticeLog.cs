﻿using System;
using System.ComponentModel.DataAnnotations;
using Volo.Abp.Domain.Entities;

namespace PasteTimer.noticemodels
{

    /// <summary>
    /// 发送记录
    /// </summary>
    public class NoticeLog : Entity<int>
    {

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateDate { get; set; } = DateTimeOffset.Now.Date;

        /// <summary>
        /// 消息代码
        /// </summary>
        [MaxLength(16)]
        public string Code { get; set; }

        /// <summary>
        /// 事件对象ID
        /// </summary>
        public int ObjId { get; set; }

        /// <summary>
        /// 消息正文
        /// </summary>
        public string Body { get; set; }

        /// <summary>
        /// 发生次数
        /// </summary>
        public int Time { get; set; }

        /// <summary>
        /// 接收者
        /// </summary>
        public int RevicerId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool Success { get; set; } = false;

    }
}
