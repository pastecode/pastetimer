﻿using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Domain.Entities;

namespace PasteTimer.noticemodels
{

    /// <summary>
    /// 接收者
    /// </summary>
    public class RevicerInfo : Entity<int>
    {
        /// <summary>
        /// 接收者名称
        /// </summary>
        [Comment("接收者名称")]
        [MaxLength(16)]
        public string Name { get; set; }

        /// <summary>
        /// 状态是否可用
        /// </summary>
        public bool IsEnable { get; set; } = true;

        /// <summary>
        /// 推送地址
        /// </summary>
        [Comment("推送地址")]
        [MaxLength(256)]
        public string Url { get; set; }

        /// <summary>
        /// |all|taskexception|taskfail|
        /// </summary>
        [Comment("接收消息类型")]
        public string Codes { get; set; }

        /// <summary>
        /// 绑定用户
        /// </summary>
        public int UserId { get; set; }
    }

}
