﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PasteTimer
{

    /// <summary>
    /// 竞选集群管理者信息
    /// </summary>
    public class SlaveVoteInfo { 

        /// <summary>
        /// 当前时间戳 毫秒
        /// </summary>
      public long time { get; set; }

        /// <summary>
        /// 提交我的信息
        /// </summary>
      public SlaveMaster Master { get; set; }

    }

    /// <summary>
    /// 当前管理者信息
    /// </summary>
    public class SlaveMaster
    {
        /// <summary>
        /// 节点ID
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// 竞选时间
        /// </summary>
        public long time { get; set; }

        /// <summary>
        /// 地址信息
        /// </summary>
        public string url { get; set; }

        /// <summary>
        /// 自己一半密钥，对方一半密钥
        /// </summary>
        public string token { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class SlaveHealthModel
    {
        /// <summary>
        /// 节点的master的id
        /// </summary>
        public int masterid { get; set; }
        /// <summary>
        /// 节点的master的url
        /// </summary>
        public string masterurl { get; set; }
        /// <summary>
        /// 节点的master的vote时间
        /// </summary>
        public int mastertime { get; set; }
        /// <summary>
        /// 节点的id
        /// </summary>
        public int id { get; set; }
        /// <summary>
        /// 节点的地址
        /// </summary>
        public string url { get; set; }
    }


    /// <summary>
    /// 
    /// </summary>
    public class SlaveCallBack
    {
        /// <summary>
        /// 200 确认成功 213旧的还能用
        /// </summary>
        public int code { get; set; } = 200;
        /// <summary>
        /// 213的时候为SlaveMater.String
        /// </summary>
        public string message { get; set; } = "执行成功";
    }

    ///// <summary>
    ///// 节点健康检查信息
    ///// </summary>
    //public class SlaveHealth
    //{
    //    /// <summary>
    //    /// 最后成功交互时间 秒
    //    /// </summary>
    //    public long last { get; set; }
    //    /// <summary>
    //    /// 失败的计数
    //    /// </summary>
    //    public int errcount { get; set; }
    //}

    ///// <summary>
    ///// 节点之间传递的包裹
    ///// </summary>
    //public class SlavePackage
    //{
    //    /// <summary>
    //    /// 来源slave.id
    //    /// </summary>
    //    public int fromid { get; set; }
    //    /// <summary>
    //    /// 
    //    /// </summary>
    //    public int obj1 { get; set; }
    //    /// <summary>
    //    /// 
    //    /// </summary>
    //    public string obj2 { get; set; }
    //    /// <summary>
    //    /// 
    //    /// </summary>
    //    public string obj3 { get; set; }
    //}

    // /api/task/slave/Action
    //1.voteadd
    //2.addplan
    //3.votecannel
    //4.findslave 查找自己
    //5.link 检查是否可以链接上，一般是其他任务触发
    //6.health 健康检查 附带自己的信息给master 
    //7.

    //4.如果有一个链接不上，咋办？ 但是有人可以链接上
    //5.

    //开局给自己一个随机guid 然后遍历node节点去寻找，如果没有找到?抛出异常或者一直找下去
    //slave-master -->sure master 
    //中途加入slave -->vote -->backmasterinfo -->slave告知master 我加入
    //start --> find myself --> votemaster -->ok --->findmaster -> join it;
    //


    /// <summary>
    /// 节点状态
    /// </summary>
    public enum SlaveState
    {
        /// <summary>
        /// 离线了
        /// </summary>
        offline,
        /// <summary>
        /// 在集群中
        /// </summary>
        ingroup,
        /// <summary>
        /// 在线
        /// </summary>
        online,
        /// <summary>
        /// 引发异常
        /// </summary>
        exception,
        /// <summary>
        /// 链接中
        /// </summary>
        linking,
        /// <summary>
        /// 建立链接
        /// </summary>
        linked

    }
}
