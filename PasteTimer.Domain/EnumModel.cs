﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PasteTimer
{
    /// <summary>
    /// 任务运行状态
    /// </summary>
    public enum TaskStatus
    {
        /// <summary>
        /// 等待运行
        /// </summary>
        waitrun = 1,
        /// <summary>
        /// 运行中
        /// </summary>
        running = 2,
        /// <summary>
        /// 成功
        /// </summary>
        success = 200,
        /// <summary>
        /// 失败
        /// </summary>
        failed = 500,
        /// <summary>
        /// 取消
        /// </summary>
        cannel = 5,
        /// <summary>
        /// 异常停止
        /// </summary>
        exception = 6
    }

    /// <summary>
    /// 拉取模式
    /// </summary>
    public enum RunWay
    {
        /// <summary>
        /// 程序集模式
        /// </summary>
        Assembly = 1,
        /// <summary>
        /// 远程程序集
        /// </summary>
        RemoteAssembly = 3,
        /// <summary>
        /// HTTP请求模式
        /// </summary>
        HttpApi = 2
    }

    /// <summary>
    /// 
    /// </summary>
    public enum TaskType
    {
        /// <summary>
        /// 未知类型 表示不过滤
        /// </summary>
        Unknow=0,
        /// <summary>
        /// 示例:2022-08-09 14:09:12执行
        /// </summary>
        TimeSpan = 1,
        /// <summary>
        /// 示例：每隔5秒执行一次
        /// </summary>
        Tick = 2,
        /// <summary>
        /// 示例：每小时的(.*)01:00
        /// </summary>
        OnTime = 3,
        /// <summary>
        /// 直接发送给队列执行 这种主动吊起，需要知道ID 参数动态
        /// </summary>
        Queue = 4
    }

    /// <summary>
    /// 节点状态
    /// </summary>
    public enum NodeStatus
    {
        /// <summary>
        /// 运行中
        /// </summary>
        running = 1,
        /// <summary>
        /// 默认模式
        /// </summary>
        normal = 0,
        /// <summary>
        /// 异常
        /// </summary>
        exception = 2,
        /// <summary>
        /// 错误
        /// </summary>
        error = 4,
        /// <summary>
        /// 停止
        /// </summary>
        stop=5
    }

}
