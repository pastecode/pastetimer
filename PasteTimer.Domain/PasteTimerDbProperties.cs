﻿namespace PasteTimer
{
    /// <summary>
    /// 
    /// </summary>
    public static class PasteTimerDbProperties
    {
        /// <summary>
        /// 
        /// </summary>
        public static string DbTablePrefix { get; set; } = "TM";

        /// <summary>
        /// 
        /// </summary>
        public static string DbSchema { get; set; } = null;

        /// <summary>
        /// Pgsql
        /// </summary>
        public const string ConnectionStringName = "MainConnectionString";

        /// <summary>
        /// 
        /// </summary>
        public const string SqlserverConnectionStringName = "SqlserverConnectionString";

        /// <summary>
        /// 
        /// </summary>
        public const string SqliteConnectionStringName = "SqliteConnectionString";

        /// <summary>
        /// 
        /// </summary>
        public const string MysqlConnectionStringName = "MysqlConnectionString";

        /// <summary>
        /// 
        /// </summary>
        public const string OnlyReadConnectionStringName = "ReadConnectionString";
    }
}
