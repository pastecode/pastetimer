﻿using System;
using Volo.Abp.Domain.Entities;

namespace PasteTimer.taskmodels
{
    /// <summary>
    /// 
    /// </summary>
    public class ReportInfo : Entity<int>
    {
        /// <summary>
        /// 任务ID非节点ID，改版了
        /// </summary>
        public int NodeId { get; set; }

        /// <summary>
        /// 任务ID
        /// </summary>
        public int TaskId { get; set; }

        /// <summary>
        /// 执行总数
        /// </summary>
        public int Total { get; set; }

        /// <summary>
        /// 成功数
        /// </summary>
        public int Success { get; set; }

        /// <summary>
        /// 失败数
        /// </summary>
        public int Failed { get; set; }

        /// <summary>
        /// 时间
        /// </summary>
        public DateTime DataDate { get; set; }

        /// <summary>
        /// 报表类型 0小时 1天 2月
        /// </summary>
        public int DataType { get; set; }
    }
}
