﻿using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Domain.Entities;

namespace PasteTimer.taskmodels
{
    /// <summary>
    /// 节点信息
    /// </summary>
    public class NodeInfo : Entity<int>
    {

        /// <summary>
        /// 群组名称 表示同名的可以共同分布
        /// </summary>
        [MaxLength(16)]
        [Comment("节点分组")]
        public string Group { get; set; } = "default";

        /// <summary>
        /// 节点地址 http://www.www.com/或者https://www.abc.com:8765/
        /// </summary>
        [MaxLength(256)]
        [Comment("节点地址")]
        public string Url { get; set; }

        /// <summary>
        /// 运行状态
        /// </summary>
        [Comment("运行状态")]
        public NodeStatus Status { get; set; }

        /// <summary>
        /// 可用
        /// </summary>
        [Comment("可用")]
        public bool IsEnable { get; set; }

        /// <summary>
        /// 共执行任务数
        /// </summary>
        [Comment("共执行任务数")]
        public int TotalTask { get; set; }

        /// <summary>
        /// 总失败任务数
        /// </summary>
        [Comment("总失败任务数")]
        public int TotalFailed { get; set; }

        /// <summary>
        /// 拉取密钥 向节点拉取的时候，节点校验信息来源是否合法
        /// </summary>
        [MaxLength(128)]
        [Comment("拉取密钥")]
        public string JoinToken { get; set; }

        /// <summary>
        /// 接口安全校验 32
        /// </summary>
        [MaxLength(32)]
        public string Token { get; set; }

        /// <summary>
        /// 绑定给哪个用户
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// 客户端码，多个用,隔开 256
        /// </summary>
        [MaxLength(256)]
        public string ClientCodes { get; set; } = "";
    }
}
