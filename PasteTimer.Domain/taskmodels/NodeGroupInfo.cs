﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Domain.Entities;

namespace PasteTimer.taskmodels
{
    /// <summary>
    /// 节点分组信息
    /// </summary>
    [Index(nameof(Name),IsUnique =true)]
    public class NodeGroupInfo:Entity<int>
    {
        /// <summary>
        /// 分组名称 16
        /// </summary>
        [MaxLength(16)]
        public string Name { get; set; }

        /// <summary>
        /// 描述 128
        /// </summary>
        [MaxLength(128)]
        public string Description { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 创建者
        /// </summary>
        public int UserId { get; set; }

        //自动创建，用于下拉，其他不处理
    }
}
