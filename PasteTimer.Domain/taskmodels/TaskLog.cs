﻿using System;
using Volo.Abp.Domain.Entities;

namespace PasteTimer.taskmodels
{
    /// <summary>
    /// 任务日志
    /// </summary>
    [CType("long")]
    public class TaskLog : Entity<long>
    {
        /// <summary>
        /// 任务
        /// </summary>
        public int TaskId { get; set; }

        /// <summary>
        /// 时间
        /// </summary>
        public DateTime CreateDate { get; set; } = DateTime.UtcNow;

        /// <summary>
        /// 节点
        /// </summary>
        public int NodeId { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public TaskStatus Status { get; set; } = TaskStatus.waitrun;

        /// <summary>
        /// 耗时 秒
        /// </summary>
        public int Durtion { get; set; }

        /// <summary>
        /// 执行反馈
        /// </summary>
        public string Message { get; set; } = "";

    }
}
