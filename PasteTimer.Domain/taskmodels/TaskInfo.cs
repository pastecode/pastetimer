﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Domain.Entities;

namespace PasteTimer.taskmodels
{
    /// <summary>
    /// 任务信息
    /// </summary>
    public class TaskInfo : Entity<int>
    {
        /// <summary>
        /// 群组名称 允许发送给哪些组的节点执行 *:不挑剔/default:发送给default的组的节点执行/abc,bbc:可以发送给这2个组的节点执行
        /// </summary>
        [MaxLength(64)]
        [Comment("群组名称")]
        public string Groups { get; set; } = "default";

        /// <summary>
        /// 任务名称
        /// </summary>
        [MaxLength(24)]
        [Comment("任务名称")]
        public string Name { get; set; } = "";

        /// <summary>
        /// 程序集名称
        /// </summary>
        [MaxLength(64)]
        [Comment("程序集名称")]
        public string Assembly { get; set; } = "";

        /// <summary>
        /// 任务类型
        /// </summary>
        [Comment("任务类型")]
        public TaskType TaskType { get; set; }

        /// <summary>
        /// 拉取模式 http模式如何传参?一律采用POST模式 全部body传参？
        /// </summary>
        [Comment("拉取模式")]
        public RunWay RunWay { get; set; }

        /// <summary>
        /// 程序集压缩文件地址 如果是内置的则为空  是否可以动态依赖注入？ Project.Service.dll-->project.service.zip -->
        /// </summary>
        [MaxLength(256)]
        [Comment("程序集压缩文件地址")]
        public string AssemblyZip { get; set; } = "";

        /// <summary>
        /// 文件版本，上传一次改变一次
        /// </summary>
        [Comment("上传一次改变一次")]
        public int FileVersion { get; set; }

        /// <summary>
        /// 任务启动时间
        /// </summary>
        [Comment("任务启动时间")]
        public DateTime StartDate { get; set; } = DateTime.Now;

        /// <summary>
        /// 自动结束时间
        /// </summary>
        [Comment("自动结束时间")]
        public DateTime EndDate { get; set; } = DateTime.Now;

        /// <summary>
        /// 时间正则Wyyyy-MM-dd HH:mm:ss/HH:mm:ss
        /// </summary>
        [Comment("时间正则")]
        [MaxLength(128)]
        public string TickRegex { get; set; } = "";

        /// <summary>
        ///内部 上面引申而来 计时秒数
        /// </summary>
        [Comment("内部计时秒数")]
        public int TickSecond { get; set; }

        /// <summary>
        /// 请求路径 示例：api/task/dowork http模式的请求路径
        /// </summary>
        [MaxLength(128)]
        [Comment("请求路径")]
        public string HttpPath { get; set; } = "";

        /// <summary>
        /// 任务参数
        /// </summary>
        [Comment("任务参数")]
        public string HttpBody { get; set; } = "";

        /// <summary>
        /// 状态
        /// </summary>
        public bool IsEnable { get; set; } = true;

        /// <summary>
        /// 失败后重试次数
        /// </summary>
        public int RetryCount { get; set; } = 0;

        /// <summary>
        /// 哪个用户创建的
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// 远端新建任务的时候 用于排重使用的 建议结合jointoken进行加密后获得 32
        /// </summary>
        [MaxLength(32)]
        public string MutexCode { get; set; } = "";

    }
}
