﻿using System;
using Volo.Abp.Domain.Entities;

namespace PasteTimer.taskmodels
{
    /// <summary>
    /// 
    /// </summary>
    public class TaskBindUser : Entity<int>
    {

        /// <summary>
        /// 绑定给谁
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// 哪个任务
        /// </summary>
        public int TaskId { get; set; }

        /// <summary>
        /// 创建者
        /// </summary>
        public int Create_UserId { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateDate { get; set; } = DateTime.Now;
    }
}
