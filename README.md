# PasteTimer

## 项目介绍

本项目是为了解决定时器问题，比如一个项目有一个定时器用于回收资源，比如你希望每天的凌晨3点执行，如果你的项目是集群部署的，那就有问题了，谁去执行这个任务，有了《PasteTimer》就可以搞定这个问题了。

支持本地定时器和远程定时器。远程一般用于集群使用，远程的有任务报表和记录等。

推荐使用Spider六边形进行发布和管理PasteTimer!

如果你需要修改代码，推荐使用PasteBuilder用于代码生成，PasteBuilder是一个VSIX的扩展，直接右键Domin的Model即可生成需要的接口和管理页面等。

以上的推荐都可以在 https://soft.pastecode.cn/Home/Soft 找到！

以我目前使用的情况来说，部署了一个PasteTimer，运行了一个月，占用服务器内存资源维持在110MB，里面任务大概20个，这个资源消耗应该算非常低了的吧！

部署发布 请查看 https://soft.pastecode.cn/Detail/17

### 技术框架
	
|名称|说明|
|:--|:--|
|ABP.vNext|基础框架|
|StackExchange.Redis|Redis缓存|
|Z.EntityFramework.Plus|EF扩展|
|PostgreSQL|数据库|
|SixLabors.ImageSharp|处理图形的|
|Swashbuckle|SwaggerAPI文档|
|Serilog|日志模块|

### 项目优势
1.有别于现有的比如Qzart Hangfire等

2.资源消耗更小

3.采用注入模式，至于单例还是瞬时啥的注入由客户端决定，是否任务重复也由客户端决定

4.源码简洁，主要就在SingleHostedService。

5.计时器的方式也简单，直接按照秒进行，进行正则匹配或者轮询匹配。

6.可控的并发量配置。并发量需要配合SQL的并发量，一个并发一般对应一个sqlpool了


# 安装运行
1.下载源码，或者发布文件，如果是源码需要VS2022 NET6.0打开后发布到文件夹，

## 如果你使用mysql数据库

1.使用VS2022打开本项目
首先在appsettings.json中修改数据库链接字符串，也就是在ConnectionStrings:MainConnectionString后面
然后在appsettings.json中修改TaskConfig:DataType的值修改为mysql

2.设置PasteTimer.HttpApi.Host为启动项目(就是右侧解决方案里面这个项目加粗了)

3.右键2的这个项目进行生成，看看是否生成成功，成功成功后继续往下执行

4.点击上方的工具栏 --》视图 --》其他窗口 --》程序包管理器控制台

5.在VS工具下方找到这个窗口，然后在这个程序包管理器控制台的里面的 默认项目(J)后面下拉选择项目为src\PasteTimer.EntityFrameworkCore

6.下下方输入如下命令add-migration databaseinit -Context MysqlDbContext

7.如果要同步数据库表结构给数据库，则继续执行命令update-database -Context MysqlDbContext

## 如果你使用其他数据库
则可以跳过上面的步骤，只需要在发布的文件夹中修改appsettings.json中的相关配置，系统在启动的时候会自己检查数据库是否创建，数据库表是否创建，默认账号是否配置等操作。

2.查看文件夹根目录是否有Dockerfile文件

3.打开appsetting.json修改对应的数据库链接字符串和redis的链接串

4.构建镜像，启动运行，比如访问的地址为http://127.0.0.1:36000/

5.打开swagger地址 比如 http://127.0.0.1:36000/swagger/

6.找到接口/api/task/user/SystemStart按照要求执行它，系统会创建数据库和默认数据 inittoken默认为空，后续可以自己修改，意思就是安装者和调用者是自己，所以可以引入一个guid啥的进行校验。

7.打开 http://127.0.0.1:36000/ 按照需求登录系统。

# 思路解析
1.使用EF，可以很好的支持多种数据库，理论上支持所有的关系型数据库，自己加对应的dll重新编译即可，个人推荐postgresql数据库，超级节省资源。

2.任务调度基于接口实现，所以需要在启动的时候注入接口的实现，具体的可以查看PasteTimer.WebTest的案例。分本地和远端的

# 尚余问题
1.数据库要是挂了，整个系统就挂了，当使用远端模式的时候。

2.Redis是否使用集群模式，自己配置。

3.master的切换过程中的任务，会有问题，因为没有Master在主持工作！这个过程虽然很短，如果master非正常停止，则会经历最大一个心跳包的时间周期。

4.关于nginx的upstream需要自己手动配置。

5.对应的客户端是否断开，查看代码，内部有自我的重试机制，超过一定次数就抛弃掉这个客户端节点了。

6.在master切换过程中部分信息可能丢失或延迟，比如noticenotify的压缩推送。

7.master发送任务给Node,如果node进行反馈的时候出错了，不一定会统计到结果，如果master发送给Node发生错误，会根据配置的进行多次尝试.

8.是否支持星期模式，比如每周六凌晨3点进行备份操作 Mon 2022-09-16 03:00:00 星期在前面然后空格，然后是时间的格式yyyyMMdd HH:mm:ss，可以参照这个进行正则匹配


#更多信息
    更多免费实用的项目可以查阅 https://soft.pastecode.cn/Home/Soft 
    如果使用遇到问题，你也可以加入我们的QQ群 296245685
