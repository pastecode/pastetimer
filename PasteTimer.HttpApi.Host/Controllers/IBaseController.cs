﻿using System.Threading.Tasks;
using Volo.Abp.AspNetCore.Mvc;

namespace PasteTimer.HttpApi.Host.Controllers
{

    public class IBaseController:AbpController
    {

        public IBaseController() { }

        /// <summary>
        /// 获取当前来源的Slave.Id
        /// </summary>
        /// <returns></returns>
        protected int CurrentSlaveId()
        {
            if (Request.Headers.ContainsKey("xtoken"))
            {
                var xtoken = base.HttpContext.Request.Headers["xtoken"].ToString();
                var strs = xtoken.Split('_');//time_fromid_toid_fromtoken_totoken
                int.TryParse(strs[1], out var fromslaveid);
                return fromslaveid;
            }
            return 0;
        }
    }
}
