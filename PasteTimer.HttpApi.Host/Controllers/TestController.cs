﻿//using Microsoft.AspNetCore.Mvc;
//using System;
//using System.Text.Json;
//using Volo.Abp.AspNetCore.Mvc;

//namespace PasteTimer.HttpApi.Host.Controllers
//{

//    /// <summary>
//    /// 
//    /// </summary>
//    [ApiController]
//    [Route("/api/task/[controller]/[action]")]
//    public class TestController : AbpController
//    {

//        /// <summary>
//        ///AbpController System.Text.Json
//        /// </summary>
//        /// <returns></returns>
//        [HttpGet]
//        public string TestJsona()
//        {
//            var a = new
//            {
//                Name = "张三",
//                age = 12,
//                ServiceMark = "from abc 123",
//                date = System.DateTime.Now
//            };
//            return JsonSerializer.Serialize(a);
//        }

//        /// <summary>
//        ///AbpController Newtonsoft
//        /// </summary>
//        /// <returns></returns>
//        [HttpGet]
//        public string TestJsonc()
//        {
//            var a = new
//            {
//                Name = "张三",
//                age = 12,
//                ServiceMark = "from abc 123",
//                date = System.DateTime.Now
//            };
//            return Newtonsoft.Json.JsonConvert.SerializeObject(a);
//        }

//        /// <summary>
//        ///AbpController Jus return
//        /// </summary>
//        /// <returns></returns>
//        [HttpGet]
//        public dynamic TestJsonb()
//        {
//            var a = new
//            {
//                Name = "张三",
//                age = 12,
//                ServiceMark = "from abc 123",
//                date = System.DateTime.Now
//            };
//            return a;
//        }

//        /// <summary>
//        /// 
//        /// </summary>
//        /// <param name="input"></param>
//        /// <returns></returns>
//        [HttpPost]
//        public dynamic TestInputString(InputDemo input)
//        {
//            var str = Newtonsoft.Json.JsonConvert.SerializeObject(input);

//            return str;
//        }

//        /// <summary>
//        /// 
//        /// </summary>
//        /// <param name="input"></param>
//        /// <returns></returns>
//        [HttpPost]
//        public dynamic TestInputModel(InputDemo input)
//        {
//            //var str = Newtonsoft.Json.JsonConvert.SerializeObject(input);
//            //var now = DateTime.Now.DayOfWeek;


//            return input;
//        }

//        [HttpGet]
//        public dynamic DateWeek()
//        {
//            //var str = Newtonsoft.Json.JsonConvert.SerializeObject(input);
//            //var now = DateTime.Now.DayOfWeek;


//            return DateTime.Now.DayOfWeek.ToString().Substring(0,3);
//        }
//    }

//    /// <summary>
//    /// 
//    /// </summary>
//    public class InputDemo
//    {
//        /// <summary>
//        /// 
//        /// </summary>
//        public DateTime date { get; set; }

//        /// <summary>
//        /// 
//        /// </summary>
//        public int age { get; set; }

//        /// <summary>
//        /// 
//        /// </summary>
//        public string Name { get; set; }
//    }
//}
