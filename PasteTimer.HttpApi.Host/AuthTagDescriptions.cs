﻿using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PasteTimer
{
    /// <summary>
    /// 
    /// </summary>
    public class AuthTagDescriptions : IDocumentFilter
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="swaggerDoc"></param>
        /// <param name="context"></param>
        public void Apply(OpenApiDocument swaggerDoc, DocumentFilterContext context)
        {
            swaggerDoc.Tags = new List<OpenApiTag>
            {
                new OpenApiTag{ Name="TaskLog",Description="任务日志"},
                new OpenApiTag{ Name="TaskInfo",Description="任务信息"},
                new OpenApiTag{ Name="NodeInfo",Description="节点信息"}
            };
        }
    }
}
