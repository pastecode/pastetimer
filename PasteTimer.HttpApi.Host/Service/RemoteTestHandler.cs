﻿using System;
using System.Threading.Tasks;
using PasteCodeTaskBase;

namespace PasteTimer
{
    /// <summary>
    /// 远程任务测试
    /// </summary>
    public class RemoteTestHandler : IPasteCodeTaskBase
    {

        /// <summary>
        /// 非本地任务，这个是给远程测试的
        /// </summary>
        /// <returns></returns>
        public override bool IsLocationService()
        {
            return false;//如果是本地模式，则使用return true;
        }

        /// <summary>
        /// 任务被唤起，自己实现任务的过程，执行完成后返回结果，直接return,如下案例
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public override async Task<PasteTaskCallBackModel> Work(PasteTaskSharedModel info)
        {
            Console.WriteLine($"{DateTime.Now} PasteTimer.Remote.Task Word!");

            await Task.Delay(1000);

            return new PasteTaskCallBackModel { code=200, message="执行成功" };
        }

    }
}
