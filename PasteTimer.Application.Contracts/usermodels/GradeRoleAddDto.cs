﻿namespace PasteTimer.usermodels
{
    public class GradeRoleAddDto
    {

        ///<summary>
        ///组别ID
        ///</summary>

        public int GradeId { get; set; }

        ///<summary>
        ///权限ID
        ///</summary>

        public int RoleId { get; set; }
    }
}
