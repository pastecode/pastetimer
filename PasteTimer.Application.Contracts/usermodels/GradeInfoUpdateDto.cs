﻿using System.ComponentModel.DataAnnotations;
using Volo.Abp.Application.Dtos;

namespace PasteTimer.usermodels
{
    /// <summary>
    /// 
    /// </summary>
    public class GradeInfoUpdateDto : EntityDto<int>
    {

        ///<summary>
        ///角色分组名称
        ///</summary>
        [MaxLength(16, ErrorMessage = "角色名称不能大于16位")]
        [Required(ErrorMessage = "角色名称不能为空")]
        public string Name { get; set; }

        ///<summary>
        ///分组描述
        ///</summary>
        [MaxLength(64, ErrorMessage = "角色描述不能大于64位")]
        public string Desc { get; set; }

        ///<summary>
        ///状态
        ///</summary>
        public bool IsEnable { get; set; } = true;
    }
}
