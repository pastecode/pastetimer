﻿using Volo.Abp.Application.Dtos;
using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace PasteTimer.usermodels
{
    public class GradeRoleListDto: EntityDto<int>
    {

            ///<summary>
            ///组别ID
            ///</summary>
            public int GradeId { get; set; }

            ///<summary>
            ///权限ID
            ///</summary>
            public int RoleId { get; set; }
    }
}
