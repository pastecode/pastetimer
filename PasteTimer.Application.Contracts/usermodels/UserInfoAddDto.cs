﻿using System.ComponentModel.DataAnnotations;

namespace PasteTimer.usermodels
{
    public class UserInfoAddDto
    {

        ///<summary>
        ///用户名
        ///</summary>
        [MaxLength(32,ErrorMessage ="用户名过长，不能超过32位")]
        [Required(ErrorMessage ="用户名不能为空")]
        public string UserName { get; set; }

        ///<summary>
        ///描述
        ///</summary>
        [MaxLength(64)]
        public string Desc { get; set; }

        ///<summary>
        ///邮箱
        ///</summary>
        [RegularExpression("[0-9,a-z]{1,24}@[a-z,0-9]{2,12}.[a-z]{2,5}", ErrorMessage = "账号格式有误xxx(1-24)@xxx(2-12).xxx(2-5)")]
        [Required(ErrorMessage = "邮箱不能为空！")]
        public string Email { get; set; }

        ///<summary>
        ///密码
        ///</summary>
        [RegularExpression("[a-z,A-Z,0-9]{3,16}",ErrorMessage ="密码规则错误，必须为字母或数字的组合,3到16位数!")]
        [Required(ErrorMessage = "密码不能为空")]
        public string PassWord { get; set; }


        ///<summary>
        ///哪个角色？
        ///</summary>
        [MaxLength(16)]
        public string Grade { get; set; }
    }
}
