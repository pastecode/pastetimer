﻿using System.ComponentModel.DataAnnotations;
using Volo.Abp.Application.Dtos;

namespace PasteTimer.usermodels
{
    /// <summary>
    /// 
    /// </summary>
    public class RoleInfoDto : EntityDto<int>
    {

        ///<summary>
        ///权限模块
        ///</summary>
        [MaxLength(16)]
        public string Model { get; set; }

        ///<summary>
        ///权限项
        ///</summary>
        [MaxLength(16)]
        public string Name { get; set; }

        ///<summary>
        ///描述
        ///</summary>
        [MaxLength(64)]
        public string Desc { get; set; }

        ///<summary>
        ///状态
        ///</summary>

        public bool IsEnable { get; set; }

        ///<summary>
        ///排序
        ///</summary>

        public int Sort { get; set; }
    }
}
