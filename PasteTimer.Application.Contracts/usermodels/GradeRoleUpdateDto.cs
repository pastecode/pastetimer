﻿using Volo.Abp.Application.Dtos;

namespace PasteTimer.usermodels
{
    /// <summary>
    /// 
    /// </summary>
    public class GradeRoleUpdateDto : EntityDto<int>
    {

        ///<summary>
        ///组别ID
        ///</summary>

        public int GradeId { get; set; }

        ///<summary>
        ///权限ID
        ///</summary>

        public int RoleId { get; set; }
    }
}
