﻿using System.ComponentModel.DataAnnotations;
using Volo.Abp.Application.Dtos;

namespace PasteTimer.usermodels
{
    /// <summary>
    /// 
    /// </summary>
    public class RoleInfoUpdateDto : EntityDto<int>
    {

        ///<summary>
        ///权限模块
        ///</summary>
        [RegularExpression("[a-z,0-9]{1,16}", ErrorMessage = "模块名称必须为英文小写或者数字")]
        [Required(ErrorMessage = "模块名称必填")]
        public string Model { get; set; }

        ///<summary>
        ///权限项
        ///</summary>
        [RegularExpression("[a-z,0-9]{1,16}", ErrorMessage = "权限名称必须为英文小写或者数字")]
        [Required(ErrorMessage = "权限名称必填")]
        public string Name { get; set; }

        ///<summary>
        ///描述
        ///</summary>
        [MaxLength(64, ErrorMessage = "权限描述最大不超过64位")]
        public string Desc { get; set; }

        ///<summary>
        ///状态
        ///</summary>

        public bool IsEnable { get; set; }

        ///<summary>
        ///排序
        ///</summary>

        public int Sort { get; set; }
    }
}
