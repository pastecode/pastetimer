﻿using Volo.Abp.Application.Dtos;
using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace PasteTimer.usermodels
{
    public class RoleInfoListDto: EntityDto<int>
    {

            ///<summary>
            ///权限模块
            ///</summary>
            [MaxLength(16)]public string Model { get; set; }

            ///<summary>
            ///权限项
            ///</summary>
            [MaxLength(16)]public string Name { get; set; }

            ///<summary>
            ///描述
            ///</summary>
            [MaxLength(64)]public string Desc { get; set; }

            ///<summary>
            ///状态
            ///</summary>
            public bool IsEnable { get; set; }

            ///<summary>
            ///排序
            ///</summary>
            public int Sort { get; set; }
    }

    public class InputQueryRoleInfo : InputQueryBase
    {
        /// <summary>
        /// 
        /// </summary>
        public int groupid { get; set; }
    }
}
