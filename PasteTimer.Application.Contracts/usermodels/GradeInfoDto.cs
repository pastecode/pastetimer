﻿using System.ComponentModel.DataAnnotations;
using Volo.Abp.Application.Dtos;

namespace PasteTimer.usermodels
{
    /// <summary>
    /// 
    /// </summary>
    public class GradeInfoDto : EntityDto<int>
    {

        ///<summary>
        ///角色分组名称
        ///</summary>
        [MaxLength(16)]
        public string Name { get; set; }

        ///<summary>
        ///分组描述
        ///</summary>
        [MaxLength(64)]
        public string Desc { get; set; }

        ///<summary>
        ///状态
        ///</summary>

        public bool IsEnable { get; set; }
    }
}
