﻿using System;
using Volo.Abp.Application.Dtos;

namespace PasteTimer.taskmodels
{

    /// <summary>
    /// 
    /// </summary>
    public class ReportInfoAddDto
    {

        ///<summary>
        /// 
        ///</summary>

        public int NodeId { get; set; }

        ///<summary>
        /// 
        ///</summary>

        public int Total { get; set; }

        ///<summary>
        /// 
        ///</summary>

        public int Success { get; set; }

        ///<summary>
        /// 
        ///</summary>

        public int Failed { get; set; }

        ///<summary>
        ///时间
        ///</summary>

        public DateTime DataDate { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class ReportInfoDto : EntityDto<int>
    {

        ///<summary>
        /// 
        ///</summary>

        public int NodeId { get; set; }

        ///<summary>
        /// 
        ///</summary>

        public int Total { get; set; }

        ///<summary>
        /// 
        ///</summary>

        public int Success { get; set; }

        ///<summary>
        /// 
        ///</summary>

        public int Failed { get; set; }

        ///<summary>
        ///时间
        ///</summary>

        public DateTime DataDate { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class ReportInfoListDto : EntityDto<int>
    {

        ///<summary>
        /// 
        ///</summary>
        public int NodeId { get; set; }

        ///<summary>
        /// 
        ///</summary>
        public int Total { get; set; }

        ///<summary>
        /// 
        ///</summary>
        public int Success { get; set; }

        ///<summary>
        /// 
        ///</summary>
        public int Failed { get; set; }

        ///<summary>
        ///时间
        ///</summary>
        public DateTime DataDate { get; set; }
    }

    public class InputReportModel : InputQueryBase
    {
        /// <summary>
        /// 
        /// </summary>
        public DateTime ? sdate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime ? edate { get; set; }

        /// <summary>
        /// 查询某一个任务的报表
        /// </summary>
        public int taskid { get; set; } = 0;

        /// <summary>
        ///  0小时 1天 2月 -1不分类?
        /// </summary>
        public int date_type { get; set; } = -1;
    }

    /// <summary>
    /// 
    /// </summary>
    public class ReportInfoUpdateDto : EntityDto<int>
    {

        ///<summary>
        /// 
        ///</summary>

        public int NodeId { get; set; }

        ///<summary>
        /// 
        ///</summary>

        public int Total { get; set; }

        ///<summary>
        /// 
        ///</summary>

        public int Success { get; set; }

        ///<summary>
        /// 
        ///</summary>

        public int Failed { get; set; }

        ///<summary>
        ///时间
        ///</summary>

        public DateTime DataDate { get; set; }
    }
}
