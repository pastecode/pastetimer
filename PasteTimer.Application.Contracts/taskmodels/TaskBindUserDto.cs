﻿using System;
using System.ComponentModel.DataAnnotations;
using Volo.Abp.Application.Dtos;

namespace PasteTimer.taskmodels
{

    ///<summary>
    ///
    ///</summary>
    public class TaskBindUserAddDto
    {

        ///<summary>
        ///绑定给谁
        ///</summary>
        [RegularExpression("[1-9][0-9]{1,9}", ErrorMessage = "输入的数据错误,{0}需要输入有效数据！")]
        public int UserId { get; set; }

        ///<summary>
        ///哪个任务
        ///</summary>
        [RegularExpression("[1-9][0-9]{1,9}", ErrorMessage = "输入的数据错误,{0}需要输入有效数据！")]
        public int TaskId { get; set; }

        /////<summary>
        /////创建者
        /////</summary>

        //public int Create_UserId { get; set; }
    }
    ///<summary>
    ///
    ///</summary>
    public class TaskBindUserDto : EntityDto<int>
    {

        ///<summary>
        ///绑定给谁
        ///</summary>

        public int UserId { get; set; }

        ///<summary>
        ///哪个任务
        ///</summary>

        public int TaskId { get; set; }

        ///<summary>
        ///创建者
        ///</summary>

        public int Create_UserId { get; set; }

        ///<summary>
        ///创建时间
        ///</summary>
        public DateTime CreateDate { get; set; }
    }
    ///<summary>
    ///
    ///</summary>
    public class TaskBindUserListDto : EntityDto<int>
    {

        ///<summary>
        ///绑定给谁
        ///</summary>

        public int UserId { get; set; }

        ///<summary>
        ///哪个任务
        ///</summary>

        public int TaskId { get; set; }

        ///<summary>
        ///创建者
        ///</summary>

        public int Create_UserId { get; set; }

        ///<summary>
        ///创建时间
        ///</summary>

        public DateTime CreateDate { get; set; }
    }

    ///<summary>
    ///
    ///</summary>
    public class TaskBindUserUpdateDto : EntityDto<int>
    {

        ///<summary>
        ///绑定给谁
        ///</summary>
        [RegularExpression("[1-9][0-9]{1,9}", ErrorMessage = "输入的数据错误,{0}需要输入有效数据！")]
        public int UserId { get; set; }

        ///<summary>
        ///哪个任务
        ///</summary>
        [RegularExpression("[1-9][0-9]{1,9}", ErrorMessage = "输入的数据错误,{0}需要输入有效数据！")]
        public int TaskId { get; set; }

        /////<summary>
        /////创建者
        /////</summary>
        //public int Create_UserId { get; set; }
    }
}
