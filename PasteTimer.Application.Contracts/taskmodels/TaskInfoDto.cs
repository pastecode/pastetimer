﻿using System;
using System.ComponentModel.DataAnnotations;
using Volo.Abp.Application.Dtos;

namespace PasteTimer.taskmodels
{
    /// <summary>
    /// 
    /// </summary>
    public class TaskInfoAddDto
    {

        ///<summary>
        ///群组名称 允许发送给哪些组的节点执行 *:不挑剔/default:发送给default的组的节点执行/abc,bbc:可以发送给这2个组的节点执行
        ///</summary>
        [MaxLength(64,ErrorMessage ="群组名称最长不超过64位，表示用这个群组的节点来执行任务！")]
        public string Groups { get; set; } = "default";

        /// <summary>
        /// 任务名称
        /// </summary>
        [MaxLength(24, ErrorMessage = "任务名称过程，不超过24字!")]
        public string Name { get; set; } = "";

        ///<summary>
        ///程序集名称
        ///</summary>
        [MaxLength(64)]
        public string Assembly { get; set; } = "";

        ///<summary>
        ///任务类型
        ///</summary>
        public TaskType TaskType { get; set; } = TaskType.Tick;

        ///<summary>
        ///拉取模式 http模式如何传参?一律采用POST模式 全部body传参？
        ///</summary>
        public RunWay RunWay { get; set; } = RunWay.Assembly;

        ///<summary>
        ///程序集压缩文件地址 如果是内置的则为空  是否可以动态依赖注入？ Project.Service.dll-->project.service.zip -->
        ///</summary>
        [MaxLength(256)]
        public string AssemblyZip { get; set; } = "";

        ///<summary>
        ///任务启动时间
        ///</summary>
        public DateTime StartDate { get; set; } = DateTime.Now;

        ///<summary>
        ///自动结束时间
        ///</summary>
        public DateTime EndDate { get; set; } = DateTime.Now.AddMonths(1);

        ///<summary>
        ///时间正则Wyyyy-MM-dd HH:mm:ss/HH:mm:ss
        ///表示一个点，或者是时间符合这个规则
        ///</summary>
        [MaxLength(128,ErrorMessage ="规则最长128，示例yyyy-MM-dd HH:mm:ss/HH:mm:ss/Sunyyyy-MM-dd HH:mm:ss")]
        public string TickRegex { get; set; } = "00:00:10";

        ///<summary>
        ///请求路径 示例：api/task/dowork http模式的请求路径
        ///</summary>
        [MaxLength(128,ErrorMessage ="自定义路径最大128位，请重新输入！")]
        public string HttpPath { get; set; } = "";

        ///<summary>
        ///Http模式的请求Body
        ///</summary>
        public string HttpBody { get; set; } = "";

        /// <summary>
        /// 状态
        /// </summary>
        public bool IsEnable { get; set; } = true;

        /// <summary>
        /// 
        /// </summary>
        public int RetryCount { get; set; } = 0;

        /// <summary>
        /// 用于远端新建任务的时候校验，必须要和对应节点node的jointoken一致
        /// </summary>
        public string JoinToken { get; set; } = "";

        /// <summary>
        /// 远端新建任务的时候 用于排重使用的 建议结合jointoken进行加密后获得 32
        /// </summary>
        [MaxLength(32,ErrorMessage ="唯一码最长32位，请重新输入！")]
        public string MutexCode { get; set; } = "";
    }

    /// <summary>
    /// 
    /// </summary>
    public class TaskInfoDto : EntityDto<int>
    {

        ///<summary>
        ///群组名称 允许发送给哪些组的节点执行 *:不挑剔/default:发送给default的组的节点执行/abc,bbc:可以发送给这2个组的节点执行
        ///</summary>
        [MaxLength(64)]
        public string Groups { get; set; }

        ///<summary>
        ///程序集名称
        ///</summary>
        [MaxLength(64)]
        public string Assembly { get; set; }

        ///<summary>
        ///任务类型
        ///</summary>

        public TaskType TaskType { get; set; }

        ///<summary>
        ///拉取模式 http模式如何传参?一律采用POST模式 全部body传参？
        ///</summary>

        public RunWay RunWay { get; set; }

        ///<summary>
        ///程序集压缩文件地址 如果是内置的则为空  是否可以动态依赖注入？ Project.Service.dll-->project.service.zip -->
        ///</summary>
        [MaxLength(256)]
        public string AssemblyZip { get; set; }

        /// <summary>
        /// 文件版本，上传一次改变一次
        /// </summary>
        public int FileVersion { get; set; }

        ///<summary>
        ///任务启动时间
        ///</summary>

        public DateTime StartDate { get; set; }

        ///<summary>
        ///自动结束时间
        ///</summary>

        public DateTime EndDate { get; set; }

        ///<summary>
        ///时间正则Wyyyy-MM-dd HH:mm:ss/HH:mm:ss
        ///</summary>
        [MaxLength(128)]
        public string TickRegex { get; set; }

        ///<summary>
        /// 内部 上面引申而来 计时秒数 
        ///</summary>

        public int TickSecond { get; set; }

        ///<summary>
        ///请求路径 示例：api/task/dowork http模式的请求路径
        ///</summary>
        [MaxLength(128)]
        public string HttpPath { get; set; }

        ///<summary>
        ///Http模式的请求Body
        ///</summary>
        public string HttpBody { get; set; } = "";

        /// <summary>
        /// 
        /// </summary>
        public bool IsEnable { get; set; } = true;

        /// <summary>
        /// 扩展 任务流水ID
        /// </summary>
        public int TaskLogId { get; set; }

        /// <summary>
        /// 扩展 执行的节点ID
        /// </summary>
        public int NodeId { get; set; }

        /// <summary>
        /// 扩展 任务流水串号
        /// </summary>
        public string TaskLogGuid { get; set; }
        /// <summary>
        /// 扩展 任务时间
        /// </summary>
        public DateTime TaskDate { get; set; }
        /// <summary>
        /// 扩展 任务时间 任务设定执行的时间
        /// </summary>
        public long TaskTime { get; set; }

        /// <summary>
        /// 扩展 计划时间 重试的话这个时间会变，是执行时间
        /// </summary>
        public long PlanTime { get; set; }

        /// <summary>
        /// 失败后重试次数
        /// </summary>
        public int RetryCount { get; set; } = 0;
    }

    /// <summary>
    /// 
    /// </summary>
    public class TaskInfoListDto : EntityDto<int>
    {

        ///<summary>
        ///群组名称 允许发送给哪些组的节点执行 *:不挑剔/default:发送给default的组的节点执行/abc,bbc:可以发送给这2个组的节点执行
        ///</summary>
        [MaxLength(64)]
        public string Groups { get; set; } = "";

        /// <summary>
        /// 任务名称
        /// </summary>
        [MaxLength(24)]
        public string Name { get; set; } = "";

        ///<summary>
        ///程序集名称
        ///</summary>
        [MaxLength(64)]
        public string Assembly { get; set; } = "";

        ///<summary>
        ///任务类型
        ///</summary>
        public TaskType TaskType { get; set; }

        ///<summary>
        ///拉取模式 http模式如何传参?一律采用POST模式 全部body传参？
        ///</summary>
        public RunWay RunWay { get; set; }

        ///<summary>
        ///程序集压缩文件地址 如果是内置的则为空  是否可以动态依赖注入？ Project.Service.dll-->project.service.zip -->
        ///</summary>
        [MaxLength(256)]
        public string AssemblyZip { get; set; } = "";

        ///<summary>
        ///任务启动时间
        ///</summary>
        public DateTime StartDate { get; set; } = DateTime.Now;

        ///<summary>
        ///自动结束时间
        ///</summary>
        public DateTime EndDate { get; set; } = DateTime.Now;

        ///<summary>
        ///时间正则Wyyyy-MM-dd HH:mm:ss/HH:mm:ss
        ///</summary>
        [MaxLength(128)]
        public string TickRegex { get; set; } = "";

        ///<summary>
        /// 内部 上面引申而来 计时秒数 
        ///</summary>
        public int TickSecond { get; set; }

        ///<summary>
        ///请求路径 示例：api/task/dowork http模式的请求路径
        ///</summary>
        [MaxLength(128)]
        public string HttpPath { get; set; } = "";

        ///<summary>
        ///Http模式的请求Body
        ///</summary>
        public string HttpBody { get; set; } = "";

        /// <summary>
        /// 
        /// </summary>
        public bool IsEnable { get; set; } = true;
    }

    /// <summary>
    /// 
    /// </summary>
    public class InputQueryTaskModel:InputQueryBase
    {

        /// <summary>
        /// -1不参与过滤 1正常的 0禁用的
        /// </summary>
        public int state { get; set; } = -1;

        /// <summary>
        /// 类型
        /// </summary>
        public TaskType task_type { get; set; } = TaskType.Unknow;

        /// <summary>
        /// 
        /// </summary>
        public DateTime? sdate { get; set; } = null;

        /// <summary>
        /// 
        /// </summary>
        public DateTime? edate { get; set; } = null;

        /// <summary>
        /// 群组
        /// </summary>
        public string group { get; set; }
             
    }

    public class TaskInfoUpdateDto : EntityDto<int>
    {

        ///<summary>
        ///群组名称 允许发送给哪些组的节点执行 *:不挑剔/default:发送给default的组的节点执行/abc,bbc:可以发送给这2个组的节点执行
        ///</summary>
        [MaxLength(64)]
        public string Groups { get; set; }

        /// <summary>
        /// 任务名称
        /// </summary>
        [MaxLength(24, ErrorMessage = "任务名称过程，不超过24字!")]
        public string Name { get; set; } = "";
        ///<summary>
        ///程序集名称
        ///</summary>
        [MaxLength(64)]
        public string Assembly { get; set; }

        ///<summary>
        ///任务类型
        ///</summary>

        public TaskType TaskType { get; set; }

        ///<summary>
        ///拉取模式 http模式如何传参?一律采用POST模式 全部body传参？
        ///</summary>

        public RunWay RunWay { get; set; }

        ///<summary>
        ///程序集压缩文件地址 如果是内置的则为空  是否可以动态依赖注入？ Project.Service.dll-->project.service.zip -->
        ///</summary>
        [MaxLength(256)]
        public string AssemblyZip { get; set; }

        ///<summary>
        ///任务启动时间
        ///</summary>

        public DateTime StartDate { get; set; }

        ///<summary>
        ///自动结束时间
        ///</summary>

        public DateTime EndDate { get; set; }

        ///<summary>
        ///时间正则Wyyyy-MM-dd HH:mm:ss/HH:mm:ss
        ///</summary>
        [MaxLength(128)]
        public string TickRegex { get; set; }


        ///<summary>
        ///请求路径 示例：api/task/dowork http模式的请求路径
        ///</summary>
        [MaxLength(128)]
        public string HttpPath { get; set; }

        ///<summary>
        ///Http模式的请求Body
        ///</summary>

        public string HttpBody { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool IsEnable { get; set; } = true;
        /// <summary>
        /// 
        /// </summary>
        public int RetryCount { get; set; } = 0;
    }
}
