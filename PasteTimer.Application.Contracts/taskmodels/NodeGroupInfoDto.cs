﻿using System.ComponentModel.DataAnnotations;
using Volo.Abp.Application.Dtos;

namespace PasteTimer.taskmodels
{

    /////<summary>
    /////节点分组信息
    /////</summary>
    //public class NodeGroupInfoAddDto
    //{

    //    ///<summary>
    //    ///分组名称 16
    //    ///</summary>
    //    [MaxLength(16)]
    //    public string Name { get; set; }

    //    ///<summary>
    //    ///描述 128
    //    ///</summary>
    //    [MaxLength(128)]
    //    public string Description { get; set; }

    //    ///<summary>
    //    ///排序
    //    ///</summary>

    //    public int Sort { get; set; }

    //    ///<summary>
    //    ///创建者
    //    ///</summary>

    //    public int UserId { get; set; }
    //}
    /////<summary>
    /////节点分组信息
    /////</summary>
    //public class NodeGroupInfoDto : EntityDto<int>
    //{

    //    ///<summary>
    //    ///分组名称 16
    //    ///</summary>
    //    [MaxLength(16)]
    //    public string Name { get; set; }

    //    ///<summary>
    //    ///描述 128
    //    ///</summary>
    //    [MaxLength(128)]
    //    public string Description { get; set; }

    //    ///<summary>
    //    ///排序
    //    ///</summary>

    //    public int Sort { get; set; }

    //    ///<summary>
    //    ///创建者
    //    ///</summary>

    //    public int UserId { get; set; }
    //}
    ///<summary>
    ///节点分组信息
    ///</summary>
    public class NodeGroupInfoListDto : EntityDto<int>
    {

        ///<summary>
        ///分组名称 16
        ///</summary>
        [MaxLength(16)]
        public string Name { get; set; }

        ///<summary>
        ///描述 128
        ///</summary>
        [MaxLength(128)]
        public string Description { get; set; }

        ///<summary>
        ///排序
        ///</summary>

        public int Sort { get; set; }

        ///<summary>
        ///创建者
        ///</summary>

        public int UserId { get; set; }
    }

    /////<summary>
    /////节点分组信息
    /////</summary>
    //public class NodeGroupInfoUpdateDto : EntityDto<int>
    //{

    //    ///<summary>
    //    ///分组名称 16
    //    ///</summary>
    //    [MaxLength(16)]
    //    public string Name { get; set; }

    //    ///<summary>
    //    ///描述 128
    //    ///</summary>
    //    [MaxLength(128)]
    //    public string Description { get; set; }

    //    ///<summary>
    //    ///排序
    //    ///</summary>

    //    public int Sort { get; set; }

    //    ///<summary>
    //    ///创建者
    //    ///</summary>

    //    public int UserId { get; set; }
    //}
}
