﻿using System.ComponentModel.DataAnnotations;
using Volo.Abp.Application.Dtos;

namespace PasteTimer.taskmodels
{

    /// <summary>
    /// 
    /// </summary>
    public class NodeInfoAddDto
    {

        ///<summary>
        ///群组名称 表示同名的可以共同分布
        ///</summary>
        [MaxLength(16)]
        [Required(ErrorMessage ="请选择分组信息，分组信息一致的表示共同承担任务执行")]
        public string Group { get; set; } = "default";

        ///<summary>
        ///节点地址 http://www.www.com/或者https://www.abc.com:8765/
        ///</summary>
        [MaxLength(256)]
        public string Url { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string JoinToken { get; set; }

        /// <summary>
        /// 客户端代码
        /// </summary>
        public string ClientCode { get; set; } = "";

    }

    /// <summary>
    /// 
    /// </summary>
    public class NodeInfoDto : EntityDto<int>
    {

        ///<summary>
        ///群组名称 表示同名的可以共同分布
        ///</summary>
        [MaxLength(16)]
        public string Group { get; set; }

        ///<summary>
        ///节点地址 http://www.www.com/或者https://www.abc.com:8765/
        ///</summary>
        [MaxLength(256)]
        public string Url { get; set; }

        ///<summary>
        ///运行状态
        ///</summary>

        public NodeStatus Status { get; set; }

        ///<summary>
        ///可用
        ///</summary>

        public bool IsEnable { get; set; }

        ///<summary>
        ///共执行任务数
        ///</summary>

        public int TotalTask { get; set; }

        ///<summary>
        ///总失败任务数
        ///</summary>

        public int TotalFailed { get; set; }

        ///<summary>
        ///拉取密钥 向节点拉取的时候，节点校验信息来源是否合法
        ///</summary>
        [MaxLength(128)]
        public string JoinToken { get; set; }

        /// <summary>
        /// 扩展 最后通讯时间
        /// </summary>
        public long LastLink { get; set; } = 0;

        /// <summary>
        /// 扩展 错误次数
        /// </summary>
        public int TryTime { get; set; } = 0;

        /// <summary>
        /// 客户端代码
        /// </summary>
        public string ClientCode { get; set; } = "";
    }

    /// <summary>
    /// 
    /// </summary>
    public class NodeInfoListDto : EntityDto<int>
    {

        ///<summary>
        ///群组名称 表示同名的可以共同分布
        ///</summary>
        [MaxLength(16)] public string Group { get; set; }

        ///<summary>
        ///节点地址 http://www.www.com/或者https://www.abc.com:8765/
        ///</summary>
        [MaxLength(256)] public string Url { get; set; }

        ///<summary>
        ///运行状态
        ///</summary>
        public NodeStatus Status { get; set; }

        ///<summary>
        ///可用
        ///</summary>
        public bool IsEnable { get; set; }

        ///<summary>
        ///共执行任务数
        ///</summary>
        public int TotalTask { get; set; }

        ///<summary>
        ///总失败任务数
        ///</summary>
        public int TotalFailed { get; set; }

        ///<summary>
        ///拉取密钥 向节点拉取的时候，节点校验信息来源是否合法
        ///</summary>
        [MaxLength(128)] 
        public string JoinToken { get; set; }

        /// <summary>
        /// 客户端代码
        /// </summary>
        public string ClientCode { get; set; } = "";
    }

    /// <summary>
    /// 
    /// </summary>
    public class NodeInfoUpdateDto : EntityDto<int>
    {

        ///<summary>
        ///群组名称 表示同名的可以共同分布
        ///</summary>
        [MaxLength(16)]
        public string Group { get; set; }

        ///<summary>
        ///节点地址 http://www.www.com/或者https://www.abc.com:8765/
        ///</summary>
        [MaxLength(256)]
        public string Url { get; set; }

        ///<summary>
        ///运行状态
        ///</summary>
        public NodeStatus Status { get; set; } = NodeStatus.running;

        ///<summary>
        ///可用
        ///</summary>
        public bool IsEnable { get; set; }

        ///<summary>
        ///拉取密钥 向节点拉取的时候，节点校验信息来源是否合法
        ///</summary>
        [MaxLength(128)]
        public string JoinToken { get; set; }

        /// <summary>
        /// 客户端代码
        /// </summary>
        public string ClientCode { get; set; } = "";
    }
}
