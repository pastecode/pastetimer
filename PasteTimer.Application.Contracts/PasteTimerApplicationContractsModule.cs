﻿using Volo.Abp.Application;
using Volo.Abp.Authorization;
using Volo.Abp.Modularity;

namespace PasteTimer
{
    /// <summary>
    /// 
    /// </summary>
    [DependsOn(
        typeof(AbpDddApplicationContractsModule)
        )]
    public class PasteTimerApplicationContractsModule : AbpModule
    {

    }
}
