﻿using System.ComponentModel.DataAnnotations;
using Volo.Abp.Application.Dtos;

namespace PasteTimer.slavemodels
{
    /// <summary>
    /// 
    /// </summary>
    public class SlaveInfoDto : EntityDto<int>
    {

        ///<summary>
        ///当前负载端的地址
        ///</summary>
        [MaxLength(128)]
        public string Url { get; set; }

        ///<summary>
        ///总开关 
        ///</summary>

        public bool IsEnable { get; set; }

        ///<summary>
        ///负载端状态
        ///</summary>

        public SlaveState SlaveState { get; set; }

        ///<summary>
        ///是否是管理者
        ///</summary>

        public bool IsMaster { get; set; }

        ///<summary>
        ///接口安全校验
        ///</summary>
        [MaxLength(32)]
        public string Token { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string JoinToken { get; set; }

        /// <summary>
        /// 扩展 最后交互时间 时间戳ms
        /// </summary>
        public long time { get; set; }
    }
}
