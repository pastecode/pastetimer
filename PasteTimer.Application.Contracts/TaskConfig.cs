﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PasteTimer
{
    /// <summary>
    /// 系统配置信息
    /// </summary>
    public class TaskConfig
    {
        /// <summary>
        /// 加入密钥 token=time_(time_jointoken)md5
        /// </summary>
        public string PublicToken { get; set; }

        /// <summary>
        /// mysql postgresql sqlserver sqlite
        /// </summary>
        public string DataType { get; set; }

        /// <summary>
        /// 重试次数
        /// </summary>
        public int TryConnectTime { get; set; } = 5;

        ///// <summary>
        ///// single/slave
        ///// </summary>
        //public string RunModel { get; set; } = "single";

        /// <summary>
        /// 是否单例模式
        /// </summary>
        public bool SingleModel { get; set; } = true;

        /// <summary>
        /// 默认心跳检测时间
        /// </summary>
        public int HealthTime { get; set; } = 60;

        /// <summary>
        /// 管理账号邮箱
        /// </summary>
        public string Email { get; set; } = "admin@talk.com";

        /// <summary>
        /// 管理账号密码
        /// </summary>
        public string Password { get; set; } = "123456";

        /// <summary>
        /// assembly上载路径 /app/upload/assembly
        /// </summary>
        public string UploadDir { get; set; } = "/app/upload/assembly";

        /// <summary>
        /// xtoken.header的加密密钥
        /// </summary>
        public string HeadTokenSecret { get; set; } = "bhui-mki8-mjuy-76t5";
    }
}
