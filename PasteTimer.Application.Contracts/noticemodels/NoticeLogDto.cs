﻿using System;
using System.ComponentModel.DataAnnotations;
using Volo.Abp.Application.Dtos;

namespace PasteTimer.noticemodels
{
    /// <summary>
    /// 
    /// </summary>
    public class NoticeLogAddDto
    {

        ///<summary>
        ///消息代码
        ///</summary>
        [MaxLength(16)]
        public string Code { get; set; }

        ///<summary>
        ///事件对象ID
        ///</summary>
        public int ObjId { get; set; }

        ///<summary>
        ///消息正文
        ///</summary>
        public string Body { get; set; }

        ///<summary>
        ///发生次数 0表示标记，不推送
        ///</summary>
        public int Time { get; set; } = 0;
    }
    /// <summary>
    /// 
    /// </summary>
    public class NoticeLogDto : EntityDto<int>
    {

        ///<summary>
        ///创建时间
        ///</summary>

        public DateTime CreateDate { get; set; }

        ///<summary>
        ///消息代码
        ///</summary>
        [MaxLength(16)]
        public string Code { get; set; }

        ///<summary>
        ///事件对象ID
        ///</summary>

        public int ObjId { get; set; }

        ///<summary>
        ///消息正文
        ///</summary>

        public string Body { get; set; }

        ///<summary>
        ///发生次数
        ///</summary>

        public int Time { get; set; }

        ///<summary>
        ///接收者
        ///</summary>

        public int RevicerId { get; set; }
    }

    public class InputQueryNoticeLog : InputQueryBase
    {
        /// <summary>
        /// 
        /// </summary>
        public int revicerid { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class NoticeLogListDto : EntityDto<int>
    {

        ///<summary>
        ///创建时间
        ///</summary>
        public DateTime CreateDate { get; set; }

        ///<summary>
        ///消息代码
        ///</summary>
        [MaxLength(16)] public string Code { get; set; }

        ///<summary>
        ///事件对象ID
        ///</summary>
        public int ObjId { get; set; }

        ///<summary>
        ///消息正文
        ///</summary>
        public string Body { get; set; }

        ///<summary>
        ///发生次数
        ///</summary>
        public int Time { get; set; }

        ///<summary>
        ///接收者
        ///</summary>
        public int RevicerId { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class NoticeLogUpdateDto : EntityDto<int>
    {

        ///<summary>
        ///消息代码
        ///</summary>
        [MaxLength(16)]
        public string Code { get; set; }

        ///<summary>
        ///事件对象ID
        ///</summary>

        public int ObjId { get; set; }

        ///<summary>
        ///消息正文
        ///</summary>

        public string Body { get; set; }

        ///<summary>
        ///发生次数
        ///</summary>

        public int Time { get; set; }

        ///<summary>
        ///接收者
        ///</summary>

        public int RevicerId { get; set; }
    }

}
