﻿using System.ComponentModel.DataAnnotations;
using Volo.Abp.Application.Dtos;

namespace PasteTimer.noticemodels
{
    /// <summary>
    /// 
    /// </summary>
    public class MessageInfoAddDto
    {

        ///<summary>
        ///nodeoffline nodeexception taskfail taskexception
        ///</summary>
        [MaxLength(16)]
        public string Code { get; set; }

        ///<summary>
        ///介绍唯一key的计算方式
        ///</summary>
        [MaxLength(64)]
        public string Desc { get; set; }

        ///<summary>
        ///x(s) 发送频率大于1秒
        ///</summary>

        public int SendRate { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class MessageInfoDto : EntityDto<int>
    {

        ///<summary>
        ///nodeoffline nodeexception taskfail taskexception
        ///</summary>
        [MaxLength(16)]
        public string Code { get; set; }

        ///<summary>
        ///介绍唯一key的计算方式
        ///</summary>
        [MaxLength(64)]
        public string Desc { get; set; }

        ///<summary>
        ///x(s) 发送频率大于1秒
        ///</summary>

        public int SendRate { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class MessageInfoListDto : EntityDto<int>
    {

        ///<summary>
        ///nodeoffline nodeexception taskfail taskexception
        ///</summary>
        [MaxLength(16)] public string Code { get; set; }

        ///<summary>
        ///介绍唯一key的计算方式
        ///</summary>
        [MaxLength(64)] public string Desc { get; set; }

        ///<summary>
        ///x(s) 发送频率大于1秒
        ///</summary>
        public int SendRate { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class MessageInfoUpdateDto : EntityDto<int>
    {

        ///<summary>
        ///nodeoffline nodeexception taskfail taskexception
        ///</summary>
        [MaxLength(16)]
        public string Code { get; set; }

        ///<summary>
        ///介绍唯一key的计算方式
        ///</summary>
        [MaxLength(64)]
        public string Desc { get; set; }

        ///<summary>
        ///x(s) 发送频率大于1秒
        ///</summary>

        public int SendRate { get; set; }
    }
}
