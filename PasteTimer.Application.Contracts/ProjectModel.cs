﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PasteTimer
{
    /// <summary>
    /// 
    /// </summary>
    public class PasteTimerException : Exception
    {
        /// <summary>
        /// 
        /// </summary>
        public int Code { get; set; } = 500;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="code"></param>
        public PasteTimerException(string message, ExceptionCode code = ExceptionCode.ShowInfo) : base(message)
        {
            Code = (int)code;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="code"></param>
        public PasteTimerException(string message, string code) : base(message)
        {
            int.TryParse(code, out var _code);
            Code = _code;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="code"></param>
        public PasteTimerException(string message, int code) : base(message)
        {
            //int.TryParse(code, out var _code);
            Code = code;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="innerException"></param>
        public PasteTimerException(string message, Exception innerException) : base(message, innerException)
        {

        }

    }
    /// <summary>
    /// 异常分类
    /// </summary>
    public enum ExceptionCode
    {
        /// <summary>
        /// 提示性错误
        /// </summary>
        ShowInfo = 700,
        /// <summary>
        /// 输入错误
        /// </summary>
        ModelErr = 400,
        /// <summary>
        /// 没有数据 不需要提示
        /// </summary>
        EmptyInfo = 701,
        /// <summary>
        /// 未登录或者密钥错误
        /// </summary>
        UnAuth = 401,
        /// <summary>
        /// 没有权限
        /// </summary>
        UnRole = 403
    }

    public class InputQueryBase
    {
        /// <summary>
        /// 
        /// </summary>
        public int page { get; set; } = 1;

        /// <summary>
        /// 
        /// </summary>
        public int size { get; set; } = 30;

        /// <summary>
        /// 
        /// </summary>
        public string word { get; set; } = "";
    }


    /// <summary>
    /// 
    /// </summary>
    public class InputLog
    {
        /// <summary>
        /// 
        /// </summary>
        public int page { get; set; } = 1;

        /// <summary>
        /// 
        /// </summary>
        public int size { get; set; } = 30;

        /// <summary>
        /// 
        /// </summary>
        public int taskid { get; set; } = 0;

        /// <summary>
        /// 
        /// </summary>
        public int nodeid { get; set; } = 0;
    }

    ///// <summary>
    ///// 任务的回调
    ///// </summary>
    //public class TaskCallBackDto
    //{

    //    /// <summary>
    //    /// 任务ID
    //    /// </summary>
    //    public int taskid { get; set; }

    //    /// <summary>
    //    /// 执行ID
    //    /// </summary>
    //    public int logid { get; set; }

    //    /// <summary>
    //    /// 节点ID
    //    /// </summary>
    //    public int nodeid { get; set; }

    //    /// <summary>
    //    /// 执行结果 200 500 400
    //    /// </summary>
    //    public int code { get; set; }

    //    /// <summary>
    //    /// 返回结果
    //    /// </summary>
    //    public string message { get; set; } = "";

    //    /// <summary>
    //    /// 花费时长
    //    /// </summary>
    //    public int duration { get; set; } = 0;
    //}

    /// <summary>
    /// 
    /// </summary>
    public class InputLogin
    {

        /// <summary>
        /// 
        /// </summary>
        [RegularExpression("[0-9,a-z]{3,16}@[a-z,0-9]{2,12}.[a-z]{2,3}", ErrorMessage = "账号格式有误xxx(3-16)@xxx(2-12).xxx(2-3)")]
        [Required]
        public string Email { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [RegularExpression("[0-9,a-z,A-Z]{3,32}", ErrorMessage = "密码的有效位数为3~32位数的字母数字组合")]
        [Required]
        public string Password { get; set; }

        /// <summary>
        /// 图形验证码代码
        /// </summary>
        [RegularExpression("[0-9]{4,6}", ErrorMessage = "图形验证码的值为4-6位数的数字")]
        [Required]
        public string code { get; set; }

        /// <summary>
        /// 图形验证码串号
        /// </summary>
        [RegularExpression("[0-9,a-z,A-Z]{16}", ErrorMessage = "串号为一组16位数的字母或者数字的组合")]
        [Required]
        public string guid { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class InputBody
    {

        /// <summary>
        /// 
        /// </summary>
        public string body { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int id { get; set; }
    }
}
