﻿//using PasteTimer.taskmodels;
//using System;
//using System.Linq;
//using System.Threading.Tasks;
//using Volo.Abp;
//using Volo.Abp.Domain.Repositories;
//using Volo.Abp.Uow;
//using Volo.Abp.Application.Dtos;
//using Microsoft.EntityFrameworkCore;
//using Microsoft.AspNetCore.Mvc;
//using System.Linq.Dynamic.Core;
//using System.Collections.Generic;
//namespace PasteTimer.taskmodels
//{
//    /// <summary>
//    /// 节点分组信息
//    ///</summary>
//    [TypeFilter(typeof(RoleAttribute), Arguments = new object[] { "root", "root" })]
//    public class NodeGroupInfoAppService : PasteTimerAppService
//    {

//        private readonly IPasteTimerDbContext _dbContext;
//        public NodeGroupInfoAppService(IPasteTimerDbContext dbContext)
//        {
//           _dbContext=dbContext;
//        }
//        /// <summary>
//        /// 按页获取节点分组信息
//        ///</summary>
//        /// <param name="page">页码</param>
//        /// <param name="size">页大小</param>
//        /// <returns></returns>
//        [HttpGet]
//        public async Task<PagedResultDto<NodeGroupInfoListDto>> GetListAsync(int page=1,int size=20)
//        {

//            var query =_dbContext.NodeGroupInfo.Where(t => 1==1)
//            .OrderByDescending(xy => xy.Id);

//            var pagedResultDto = new PagedResultDto<NodeGroupInfoListDto>();
//            pagedResultDto.TotalCount = await query.CountAsync();
//            var userList = await query.Page(page,size).ToListAsync();
//            var temList = ObjectMapper.Map< List<NodeGroupInfo>,List<NodeGroupInfoListDto>>(userList);
//            pagedResultDto.Items = temList;
//            return pagedResultDto;
//        }

//        /// <summary>
//        /// 根据ID获取单项节点分组信息
//        ///</summary>
//        /// <param name="id"></param>
//        /// <returns></returns>
//        [HttpGet]
//        public NodeGroupInfoDto GetByIdAsync(int id)
//        {
//            var query = _dbContext.NodeGroupInfo.Where(t => t.Id == id)
//            .AsNoTracking().FirstOrDefault();
//                if(query ==null || query == default)
//            {
//                throw new PasteTimerException("没有找到这样的信息",404);
//            }
//            var temList = ObjectMapper.Map<NodeGroupInfo, NodeGroupInfoDto>(query);
//            return temList;
//        }

//        /// <summary>
//        /// 根据ID获取待更新单项信息节点分组信息
//        ///</summary>
//        /// <param name="id"></param>
//        /// <returns></returns>
//        [HttpGet]
//        public NodeGroupInfoUpdateDto GetInfoForUpdateAsync(int id)
//        {
//            var query = _dbContext.NodeGroupInfo.Where(t => t.Id == id)
//            .AsNoTracking().FirstOrDefault();
//                    if(query ==null || query == default)
//            {
//                throw new PasteTimerException("没有找到这样的信息",404);
//            }
//            var temList = ObjectMapper.Map<NodeGroupInfo, NodeGroupInfoUpdateDto>(query);
//            return temList;
//        }


//        /// <summary>
//        /// 添加一个节点分组信息
//        ///</summary>
//        /// <param name="input"></param>
//        /// <returns></returns>
//        [HttpPost]
//        public async Task<NodeGroupInfoDto> CreateItemAsync(NodeGroupInfoAddDto input)
//        {

//            var newu = ObjectMapper.Map<NodeGroupInfoAddDto, NodeGroupInfo>(input);
//            //添加自定义
//            _dbContext.Add(newu);
//            await _dbContext.SaveChangesAsync();
//            //var updated = await _repository.InsertAsync(newu,true);
//            var backinfo = ObjectMapper.Map<NodeGroupInfo, NodeGroupInfoDto>(newu);
//            return backinfo;
//        }
//        /// <summary>
//        /// 更新一个节点分组信息
//        ///</summary>
//        /// <param name="input"></param>
//        /// <returns></returns>
//        [HttpPost]
//        public async Task<NodeGroupInfoDto> UpdateItemAsync(NodeGroupInfoUpdateDto input)
//        {
//            var info =await _dbContext.NodeGroupInfo.Where(x => x.Id == input.Id)
//.FirstOrDefaultAsync();
//            if (info == null || info == default) {
//                throw new UserFriendlyException("需要查询的信息不存在","404");
//            }
//            ObjectMapper.Map<NodeGroupInfoUpdateDto, NodeGroupInfo>(input,info);
//            //var updated = await _repository.UpdateAsync(newu);
//            await _dbContext.SaveChangesAsync();
//            var backinfo = ObjectMapper.Map<NodeGroupInfo, NodeGroupInfoDto>(info);
//            return backinfo;
//        }
//    }
//}
