﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Domain.Repositories;

namespace PasteTimer.taskmodels
{

    /// <summary>
    /// 任务日志
    ///</summary>
    [TypeFilter(typeof(RoleAttribute), Arguments = new object[] { "", "" })]
    public class TaskLogAppService : PasteTimerAppService
    {


        private IPasteTimerDbContext _dbContext;


        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbContext"></param>
        public TaskLogAppService(
            IPasteTimerDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PagedResultDto<dynamic>> GetListAsync([FromQuery] InputLog input)
        {

            //SELECT t."Id", t."CreateDate", t0."Assembly" AS task, t0."Id" AS taskid, t1."Url" AS node, t1."Id" AS nodeid, t."Status" AS status, t."Durtion" AS durtion
            //            FROM "TMTaskLog" AS t
            //LEFT JOIN "TMTaskInfo" AS t0 ON t."TaskId" = t0."Id"
            //LEFT JOIN "TMNodeInfo" AS t1 ON t."NodeId" = t1."Id"
            //WHERE(t."TaskId" = @__input_taskid_0) AND(t."NodeId" = @__input_nodeid_1)
            //ORDER BY t."Id" DESC
            //LIMIT @__p_3 OFFSET @__p_2

            var basequery = _dbContext.TaskLog.Where(x => 1 == 1)
                .WhereIf(input.taskid != 0, x => x.TaskId == input.taskid)
                .WhereIf(input.nodeid != 0, x => x.NodeId == input.nodeid)
                .OrderByDescending(x => x.Id);

            var select = from c in basequery
                         join d in _dbContext.TaskInfo on c.TaskId equals d.Id into cta
                         from task in cta.DefaultIfEmpty()
                         join e in _dbContext.NodeInfo on c.NodeId equals e.Id into rtable
                         from node in rtable.DefaultIfEmpty()
                         select new
                         {
                             Id = c.Id,
                             CreateDate = c.CreateDate,
                             task = task.Assembly,
                             taskid = task.Id,
                             node = node.Url,
                             nodeid = node.Id,
                             status = c.Status,
                             durtion = c.Durtion,
                             Message = c.Message
                         };
            var _pagedto = new PagedResultDto<dynamic>();
            if (input.page == 1)
            {
                _pagedto.TotalCount = await basequery.CountAsync();
            }
            var userList = await select.Page(input.page, input.size).ToListAsync();
            _pagedto.Items = userList;
            return _pagedto;
        }

        ///// <summary>
        ///// 根据ID获取单项任务日志
        /////</summary>
        ///// <param name="id"></param>
        ///// <returns></returns>
        //[HttpGet]
        //public TaskLogDto GetByIdAsync(long id)
        //{
        //    var query = _repository.Where(t => t.Id == id)
        //    .Include(x => x.Status).FirstOrDefault();
        //    var temList = ObjectMapper.Map<TaskLog, TaskLogDto>(query);
        //    return temList;
        //}

        ///// <summary>
        ///// 根据ID获取待更新单项信息任务日志
        /////</summary>
        ///// <param name="id"></param>
        ///// <returns></returns>
        //[HttpGet]
        //public TaskLogUpdateDto GetInfoForUpdateAsync(long id)
        //{
        //    var query = _repository.Where(t => t.Id == id)
        //    .Include(x => x.Status).FirstOrDefault();
        //    var temList = ObjectMapper.Map<TaskLog, TaskLogUpdateDto>(query);
        //    return temList;
        //}


        ///// <summary>
        ///// 添加一个任务日志
        /////</summary>
        ///// <param name="input"></param>
        ///// <returns></returns>
        //[UnitOfWork(false)]
        //[HttpPost]
        //public async Task<TaskLogDto> CreateItemAsync(TaskLogAddDto input)
        //{

        //    var newu = ObjectMapper.Map<TaskLogAddDto, TaskLog>(input);
        //    newu.CreateDate = DateTime.Now; 
        //    var updated = await _repository.InsertAsync(newu, true);
        //    var backinfo = ObjectMapper.Map<TaskLog, TaskLogDto>(updated);
        //    return backinfo;
        //}
        ///// <summary>
        ///// 更新一个任务日志
        /////</summary>
        ///// <param name="input"></param>
        ///// <returns></returns>
        //[HttpPost]
        //public async Task<TaskLogDto> UpdateItemAsync(TaskLogUpdateDto input)
        //{
        //    var info =await _repository.Where(x => x.Id == input.Id).FirstOrDefaultAsync();
        //    if (info == null || info == default)
        //    {
        //        throw new UserFriendlyException("需要查询的信息不存在", "404");
        //    }
        //    ObjectMapper.Map<TaskLogUpdateDto, TaskLog>(input, info);

        //    var backinfo = ObjectMapper.Map<TaskLog, TaskLogDto>(info);
        //    return backinfo;
        //}

        ///// <summary>
        ///// 删除任务日志软删除
        /////</summary>
        ///// <param name="id"></param>
        ///// <returns></returns>
        //[HttpPost]
        //public async Task<int> DeleteItemById(int id)
        //{
        //    var aione = await _repository.GetAsync(xy => xy.Id == id);//.FirstOrDefault();
        //    if (aione != default)
        //    {
        //        //await _repository.DeleteAsync(aione);
        //    }
        //    return 1;
        //}

    }
}
