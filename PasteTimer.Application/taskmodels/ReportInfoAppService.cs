﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Domain.Repositories;

namespace PasteTimer.taskmodels
{

    /// <summary>
    ///  
    ///</summary>
    [TypeFilter(typeof(RoleAttribute), Arguments = new object[] { "data", "view" })]
    public class ReportInfoAppService : IUserAppService
    {

        private IPasteTimerDbContext _dbContext;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbcontext"></param>
        public ReportInfoAppService(IPasteTimerDbContext dbcontext)
        {
            _dbContext = dbcontext;
        }

        /// <summary>
        /// 基于分页的模式查询数据
        /// </summary>
        /// <param name="input"></param>
        /// <param name="_modelHelper"></param>
        /// <returns></returns>
        /// <exception cref="PasteTimerException"></exception>
        [HttpGet]
        public async Task<PagedResultDto<ReportInfoListDto>> Page([FromQuery]InputReportModel input, [FromServices] ModelHelper _modelHelper)
        {
            var _userid = base.ReadCurrentAdminId();
            var tasks = _modelHelper.MyTaskIdArray(_dbContext, _userid);
            if (input.taskid != 0)
            {
                if (!tasks.Contains(input.taskid))
                {
                    throw new PasteTimerException("没有当前任务的权限，无法查询数据！");
                }
            }
            if (tasks == null || tasks.Length == 0)
            {
                throw new PasteTimerException("没有绑定任务，无法查询数据！");
            }
            var _pagedto = new PagedResultDto<ReportInfoListDto>();
            var _query = _dbContext.ReportInfo.Where(x => tasks.Contains(x.NodeId) && input.sdate <= x.DataDate && x.DataDate < input.edate)
                .WhereIf(input.taskid != 0, x => x.TaskId == input.taskid)
                .WhereIf(input.date_type != -1, x => x.DataType == input.date_type);
            if (input.page == 1)
            {
                _pagedto.TotalCount = await _query.CountAsync();
            }
            var datas = await _query.OrderByDescending(x => x.DataDate).AsNoTracking().ToListAsync();
            if (datas != null && datas.Count > 0)
            {
                var dtos = ObjectMapper.Map<List<ReportInfo>, List<ReportInfoListDto>>(datas);
                _pagedto.Items = dtos;
                return _pagedto;
            }
            else
            {
                throw new PasteTimerException("没有查询到数据！", 701);
            }
        }


        /// <summary>
        /// 基于报表模式查询，需要传递参数时间
        /// </summary>
        /// <param name="input"></param>
        /// <param name="_modelHelper"></param>
        /// <returns></returns>
        /// <exception cref="PasteTimerException"></exception>
        [HttpGet]
        public async Task<List<ReportInfoListDto>> Report([FromQuery]InputReportModel input, [FromServices] ModelHelper _modelHelper)
        {
            var _userid = base.ReadCurrentAdminId();
            var tasks = _modelHelper.MyTaskIdArray(_dbContext, _userid);
            if (input.taskid != 0)
            {
                if (!tasks.Contains(input.taskid))
                {
                    throw new PasteTimerException("没有当前任务的权限，无法查询数据！");
                }
            }
            if (tasks == null || tasks.Length == 0)
            {
                throw new PasteTimerException("没有绑定任务，无法查询数据！");
            }
            var _query = await _dbContext.ReportInfo.Where(x => tasks.Contains(x.TaskId) && input.sdate <= x.DataDate && x.DataDate < input.edate)
                .WhereIf(input.taskid != 0, x => x.TaskId == input.taskid)
                .WhereIf(input.date_type != -1, x => x.DataType == input.date_type).OrderBy(x => x.DataDate).AsNoTracking().ToListAsync();

            if (_query != null && _query.Count > 0)
            {
                return ObjectMapper.Map<List<ReportInfo>, List<ReportInfoListDto>>(_query);
            }
            else
            {
                throw new PasteTimerException("没有查询到数据！", 701);
            }
        }


    }
}
