﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Volo.Abp;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Domain.Repositories;
using Volo.Abp.Uow;

namespace PasteTimer.usermodels
{

    /// <summary>
    /// 角色绑定权限
    ///</summary>
    [TypeFilter(typeof(RoleAttribute), Arguments = new object[] { "data", "view" })]
    public class GradeRoleAppService : PasteTimerAppService
    {

        //private readonly IRepository<GradeRole, int> _repository;


        private IPasteTimerDbContext _dbContext;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbcontext"></param>
        public GradeRoleAppService(IPasteTimerDbContext dbcontext
        )
        {

            _dbContext = dbcontext;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="page"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PagedResultDto<GradeRoleListDto>> GetListAsync(int page = 1, int size = 20)
        {
            var query = _dbContext.GradeRole.Where(t => 1 == 1);
            PagedResultDto<GradeRoleListDto> _pagedto = new PagedResultDto<GradeRoleListDto>();
            _pagedto.TotalCount = await query.CountAsync();
            var userList = await query.OrderByDescending(x=>x.Id).Page(page, size).ToListAsync();
            var temList = ObjectMapper.Map<List<GradeRole>, List<GradeRoleListDto>>(userList);
            _pagedto.Items = temList;
            return _pagedto;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PagedResultDto<GradeRoleListDto>> Page([FromQuery]InputQueryBase input)
        {
            var query = _dbContext.GradeRole.Where(t => 1 == 1);
            PagedResultDto<GradeRoleListDto> _pagedto = new PagedResultDto<GradeRoleListDto>();
            _pagedto.TotalCount = await query.CountAsync();
            var userList = await query.OrderByDescending(x => x.Id).Page(input.page, input.size).ToListAsync();
            var temList = ObjectMapper.Map<List<GradeRole>, List<GradeRoleListDto>>(userList);
            _pagedto.Items = temList;
            return _pagedto;
        }

        /// <summary>
        /// 根据ID获取单项角色绑定权限
        ///</summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public GradeRoleDto GetByIdAsync(int id)
        {
            var query = _dbContext.GradeRole.Where(t => t.Id == id)
                .FirstOrDefault();
            var temList = ObjectMapper.Map<GradeRole, GradeRoleDto>(query);
            return temList;
        }

        /// <summary>
        /// 根据ID获取待更新单项信息角色绑定权限
        ///</summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public GradeRoleUpdateDto GetInfoForUpdateAsync(int id)
        {
            var query = _dbContext.GradeRole.Where(t => t.Id == id)
                .FirstOrDefault();
            var temList = ObjectMapper.Map<GradeRole, GradeRoleUpdateDto>(query);
            return temList;
        }


        /// <summary>
        /// 添加一个角色绑定权限
        ///</summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [UnitOfWork(false)]
        [HttpPost]
        public async Task<GradeRoleDto> CreateItemAsync(GradeRoleAddDto input)
        {

            var newu = ObjectMapper.Map<GradeRoleAddDto, GradeRole>(input);
            //添加自定义
            //var updated = await _repository.InsertAsync(newu, true);
            _dbContext.Add(newu);
            await _dbContext.SaveChangesAsync();
            var backinfo = ObjectMapper.Map<GradeRole, GradeRoleDto>(newu);
            return backinfo;
        }
        /// <summary>
        /// 更新一个角色绑定权限
        ///</summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [UnitOfWork(false)]
        [HttpPost]
        public async Task<GradeRoleDto> UpdateItemAsync(GradeRoleUpdateDto input)
        {
            var info = await _dbContext.GradeRole.Where(x => x.Id == input.Id).FirstOrDefaultAsync();
            if (info == null || info == default)
            {
                throw new UserFriendlyException("需要查询的信息不存在", "404");
            }
            ObjectMapper.Map<GradeRoleUpdateDto, GradeRole>(input, info);
            await _dbContext.SaveChangesAsync();
            var backinfo = ObjectMapper.Map<GradeRole, GradeRoleDto>(info);
            return backinfo;
        }

        ///// <summary>
        ///// 删除角色绑定权限软删除
        /////</summary>
        ///// <param name="id"></param>
        ///// <returns></returns>
        //[HttpPost]
        //public async Task<int> DeleteItemById(int id)
        //{
        //    var aione = await _repository.GetAsync(xy => xy.Id == id);//.FirstOrDefault();
        //    if (aione != default)
        //    {
        //        //await _repository.DeleteAsync(aione);
        //    }
        //    return 1;
        //}

    }
}
