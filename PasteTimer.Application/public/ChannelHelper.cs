﻿using System.Threading.Channels;
using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;
using PasteTimer.noticemodels;

namespace PasteTimer
{
    /// <summary>
    /// 本地队列
    /// </summary>
    public class ChannelHelper:ISingletonDependency
    {

        /// <summary>
        /// 
        /// </summary>
        //private Channel<string> _orderqueue;

        private Channel<StatusModel> _statusqueue;

        private Channel<NoticeLogAddDto> _channelnotice;

        private Channel<NoticeSendModel> _channelsend;

        private Channel<SlaveEventModel> _channelslave;

        /// <summary>
        /// 
        /// </summary>
        public ChannelHelper()
        {
            //_orderqueue = Channel.CreateUnbounded<string>();
            _statusqueue = Channel.CreateUnbounded<StatusModel>();
            _channelnotice = Channel.CreateUnbounded<NoticeLogAddDto>();
            _channelsend = Channel.CreateUnbounded<NoticeSendModel>();
            _channelslave = Channel.CreateUnbounded<SlaveEventModel>();
        }

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="str"></param>
        ///// <returns></returns>
        //public async Task<int> WriteChannel(string str)
        //{
        //    await _orderqueue.Writer.WriteAsync(str);
        //    return 1;
        //}

        /// <summary>
        /// 写入状态
        /// </summary>
        /// <param name="input"></param>
        public async void WriteStatus(StatusModel input)
        {
           //await _statusqueue.Writer.WriteAsync(input);

            //是否是master 

            await _channelslave.Writer.WriteAsync(new SlaveEventModel() {
             Event="status", Object=Newtonsoft.Json.JsonConvert.SerializeObject(input)
            });

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        public async void WriteSlaveAction(SlaveEventModel input)
        {
            await _channelslave.Writer.WriteAsync(input);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="notice"></param>
        public async void WriteNotice(NoticeLogAddDto notice)
        {
            await _channelnotice.Writer.WriteAsync(notice);
        }

        /// <summary>
        /// 添加到推送列表
        /// </summary>
        /// <param name="input"></param>
        public async void WriteSend(NoticeSendModel input)
        {
            await _channelsend.Writer.WriteAsync(input);
        }

        ///// <summary>
        ///// 
        ///// </summary>
        //public Channel<string> Task { get { return _orderqueue; } }

        /// <summary>
        /// 状态变更
        /// </summary>
        public Channel<StatusModel> Status { get { return _statusqueue; } }

        /// <summary>
        /// 消息缓冲队列
        /// </summary>
        public Channel<NoticeLogAddDto> Notice { get { return _channelnotice; } }


        /// <summary>
        /// 推送队列
        /// </summary>
        public Channel<NoticeSendModel> Send { get { return _channelsend; } }

        /// <summary>
        /// 
        /// </summary>
        public Channel<SlaveEventModel> Slave { get { return _channelslave; } }
    }

    /// <summary>
    /// 
    /// </summary>
    public class StatusModel
    {
        /// <summary>
        /// 1task 2node
        /// </summary>
        public int ModelType { get; set; }

        /// <summary>
        /// 1+ 2update 3del
        /// </summary>
        public int Action { get; set; }

        /// <summary>
        /// 对象 taskinfodto/nodeinfodto
        /// </summary>
        public string body { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class SlaveEventModel
    {
        /// <summary>
        /// status stopmaster startmaster
        /// </summary>
        public string Event { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Object { get; set; }

        /// <summary>
        /// 是否来自slavecontroller
        /// </summary>
        public bool FromApi { get; set; } = false;
    }

    /// <summary>
    /// 推送队列
    /// </summary>
    public class NoticeSendModel
    {

        /// <summary>
        /// 地址
        /// </summary>
        public string url { get; set; }

        /// <summary>
        /// 内容
        /// </summary>
        public string body { get; set; }
    }
}
