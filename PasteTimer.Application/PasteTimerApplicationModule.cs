﻿using Microsoft.Extensions.DependencyInjection;
using System;
using Volo.Abp.Application;
using Volo.Abp.AutoMapper;
using Volo.Abp.Modularity;

namespace PasteTimer
{
    /// <summary>
    /// 
    /// </summary>
    [DependsOn(
        typeof(PasteTimerDomainModule),
        typeof(PasteTimerApplicationContractsModule),
        typeof(AbpDddApplicationModule),
        typeof(AbpAutoMapperModule)
        )]
    public class PasteTimerApplicationModule : AbpModule
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public override void ConfigureServices(ServiceConfigurationContext context)
        {

            Console.WriteLine("---PasteTimerApplicationModule.ConfigureServices---");

            context.Services.AddAutoMapperObjectMapper<PasteTimerApplicationModule>();
            Configure<AbpAutoMapperOptions>(options =>
            {
                //是否一一映射
                options.AddMaps<PasteTimerApplicationModule>(validate: false);

            });

        }
    }
}
