﻿using AutoMapper;
using PasteTimer.noticemodels;
using PasteTimer.slavemodels;
using PasteTimer.taskmodels;
using PasteTimer.usermodels;

namespace PasteTimer
{
    /// <summary>
    /// 
    /// </summary>
    public class PasteTimerApplicationAutoMapperProfile : Profile
    {
        /// <summary>
        /// 
        /// </summary>
        public PasteTimerApplicationAutoMapperProfile()
        {
            /* You can configure your AutoMapper mapping configuration here.
             * Alternatively, you can split your mapping configurations
             * into multiple profile classes for a better organization. */


            // *NodeInfo*:
            CreateMap<NodeInfo, NodeInfoListDto>();
            CreateMap<NodeInfo, NodeInfoDto>();
            CreateMap<NodeInfo, NodeInfoUpdateDto>();
            CreateMap<NodeInfoUpdateDto, NodeInfo>();
            CreateMap<NodeInfoAddDto, NodeInfo>();

            // *TaskInfo*:
            CreateMap<TaskInfo, TaskInfoListDto>();
            CreateMap<TaskInfo, TaskInfoDto>();
            CreateMap<TaskInfo, TaskInfoUpdateDto>();
            CreateMap<TaskInfoUpdateDto, TaskInfo>();
            CreateMap<TaskInfoAddDto, TaskInfo>();

            //// *TaskLog*:
            //CreateMap<TaskLog, TaskLogListDto>();
            //CreateMap<TaskLog, TaskLogDto>();
            //CreateMap<TaskLog, TaskLogUpdateDto>();
            //CreateMap<TaskLogUpdateDto, TaskLog>();
            //CreateMap<TaskLogAddDto, TaskLog>();

            // *ReportInfo*:
            CreateMap<ReportInfo, ReportInfoListDto>();
            CreateMap<ReportInfo, ReportInfoDto>();
            CreateMap<ReportInfo, ReportInfoUpdateDto>();
            CreateMap<ReportInfoUpdateDto, ReportInfo>();
            CreateMap<ReportInfoAddDto, ReportInfo>();

            // *UserInfo*:
            CreateMap<UserInfo, UserInfoListDto>();
            CreateMap<UserInfo, UserInfoDto>();
            CreateMap<UserInfo, UserInfoUpdateDto>();
            CreateMap<UserInfoUpdateDto, UserInfo>();
            CreateMap<UserInfoAddDto, UserInfo>();

            // *RoleInfo*:
            CreateMap<RoleInfo, RoleInfoListDto>();
            CreateMap<RoleInfo, RoleInfoDto>();
            CreateMap<RoleInfo, RoleInfoUpdateDto>();
            CreateMap<RoleInfoUpdateDto, RoleInfo>();
            CreateMap<RoleInfoAddDto, RoleInfo>();

            // *GradeInfo*:
            CreateMap<GradeInfo, GradeInfoListDto>();
            CreateMap<GradeInfo, GradeInfoDto>();
            CreateMap<GradeInfo, GradeInfoUpdateDto>();
            CreateMap<GradeInfoUpdateDto, GradeInfo>();
            CreateMap<GradeInfoAddDto, GradeInfo>();

            // *GradeRole*:
            CreateMap<GradeRole, GradeRoleListDto>();
            CreateMap<GradeRole, GradeRoleDto>();
            CreateMap<GradeRole, GradeRoleUpdateDto>();
            CreateMap<GradeRoleUpdateDto, GradeRole>();
            CreateMap<GradeRoleAddDto, GradeRole>();

            // *RevicerInfo*:
            CreateMap<RevicerInfo, RevicerInfoListDto>();
            CreateMap<RevicerInfo, RevicerInfoDto>();
            CreateMap<RevicerInfo, RevicerInfoUpdateDto>();
            CreateMap<RevicerInfoUpdateDto, RevicerInfo>();
            CreateMap<RevicerInfoAddDto, RevicerInfo>();

            // *MessageInfo*:
            CreateMap<MessageInfo, MessageInfoListDto>();
            CreateMap<MessageInfo, MessageInfoDto>();
            CreateMap<MessageInfo, MessageInfoUpdateDto>();
            CreateMap<MessageInfoUpdateDto, MessageInfo>();
            CreateMap<MessageInfoAddDto, MessageInfo>();

            // *NoticeLog*:
            CreateMap<NoticeLog, NoticeLogListDto>();
            CreateMap<NoticeLog, NoticeLogDto>();
            CreateMap<NoticeLog, NoticeLogUpdateDto>();
            CreateMap<NoticeLogUpdateDto, NoticeLog>();
            CreateMap<NoticeLogAddDto, NoticeLog>();

            CreateMap<NodeInfo, SlaveInfoDto>();



            // #TaskBindUser#
            CreateMap<TaskBindUser, TaskBindUserListDto>();
            CreateMap<TaskBindUser, TaskBindUserDto>();
            CreateMap<TaskBindUser, TaskBindUserUpdateDto>();
            CreateMap<TaskBindUserUpdateDto, TaskBindUser>();
            CreateMap<TaskBindUserAddDto, TaskBindUser>();

            // #NodeGroupInfo#
            CreateMap<NodeGroupInfo, NodeGroupInfoListDto>();
            //CreateMap<NodeGroupInfo, NodeGroupInfoDto>();
            //CreateMap<NodeGroupInfo, NodeGroupInfoUpdateDto>();
            //CreateMap<NodeGroupInfoUpdateDto, NodeGroupInfo>();
            //CreateMap<NodeGroupInfoAddDto, NodeGroupInfo>();

        }
    }
}