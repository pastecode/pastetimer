﻿//using PasteTimer.Localization;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Volo.Abp.Application.Services;

namespace PasteTimer
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class PasteTimerAppService : ApplicationService
    {
        /// <summary>
        /// 
        /// </summary>
        protected PasteTimerAppService()
        {
            //LocalizationResource = typeof(PasteTimerResource);
            ObjectMapperContext = typeof(PasteTimerApplicationModule);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public abstract class IUserAppService : ApplicationService
    {

        private HttpContext _context { get; set; } = null;

        private HttpContext _httpContext
        {
            get
            {
                if (_context == null)
                {
                    var _accessor = LazyServiceProvider.LazyGetRequiredService<IHttpContextAccessor>();
                    _context = _accessor.HttpContext;
                }
                return _context;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private TaskConfig __cg { get; set; } = null;

        /// <summary>
        /// 
        /// </summary>
        private TaskConfig _config
        {
            get
            {
                if (__cg == null)
                {
                    var _cg = LazyServiceProvider.LazyGetRequiredService<IOptions<TaskConfig>>();
                    __cg = _cg.Value;
                }
                return __cg;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected IUserAppService()
        {
            ObjectMapperContext = typeof(PasteTimerApplicationModule);
        }

        /// <summary>
        /// 获取当前登录管理员ID
        /// </summary>
        /// <returns></returns>
        protected int ReadCurrentAdminId()
        {
            var token = _httpContext.Request.Headers["token"].ToString();
            if (!string.IsNullOrEmpty(token))
            {
                var _read = token.CheckToken(_config.HeadTokenSecret);
                if (_read)
                {
                    var strs = token.Split("_");
                    int.TryParse(strs[1], out var userid);
                    return userid;
                }
            }
            return 0;
        }
    }
}
