﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Volo.Abp;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Domain.Repositories;
using Volo.Abp.Uow;

namespace PasteTimer.noticemodels
{

    /// <summary>
    /// 消息类型
    ///</summary>
    [TypeFilter(typeof(RoleAttribute), Arguments = new object[] { "data", "view" })]
    public class MessageInfoAppService : PasteTimerAppService
    {

        private readonly IRepository<MessageInfo, int> _repository;

        private readonly IPasteTimerDbContext _dbContext;

        private readonly IAppCache _appCache;


        /// <summary>
        /// 
        /// </summary>
        /// <param name="repository"></param>
        /// <param name="dbContext"></param>
        /// <param name="appCache"></param>
        public MessageInfoAppService(
            IRepository<MessageInfo, int> repository,
            IPasteTimerDbContext dbContext,
            IAppCache appCache)
        {
            _dbContext = dbContext;
            _repository = repository;
            _appCache = appCache;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="page"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        [HttpGet]
        [Obsolete]
        public async Task<PagedResultDto<MessageInfoListDto>> GetListAsync(int page = 1, int size = 20)
        {

            var query = _dbContext.MessageInfo.Where(t => 1 == 1).OrderByDescending(xy => xy.Id);
            var pagedResultDto = new PagedResultDto<MessageInfoListDto>();
            pagedResultDto.TotalCount = await query.CountAsync();
            var userList = await query.Page(page, size).ToListAsync();
            var temList = ObjectMapper.Map<List<MessageInfo>, List<MessageInfoListDto>>(userList);
            pagedResultDto.Items = temList;
            return pagedResultDto;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PagedResultDto<MessageInfoListDto>> Page([FromQuery]InputQueryBase input)
        {

            var query = _dbContext.MessageInfo.Where(t => 1 == 1).OrderByDescending(xy => xy.Id);
            var pagedResultDto = new PagedResultDto<MessageInfoListDto>();
            pagedResultDto.TotalCount = await query.CountAsync();
            var userList = await query.Page(input.page, input.size).ToListAsync();
            var temList = ObjectMapper.Map<List<MessageInfo>, List<MessageInfoListDto>>(userList);
            pagedResultDto.Items = temList;
            return pagedResultDto;
        }


        /// <summary>
        /// 根据ID获取待更新单项信息消息类型
        ///</summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public MessageInfoUpdateDto GetInfoForUpdateAsync(int id)
        {
            var query = _dbContext.MessageInfo.Where(t => t.Id == id)
                .AsNoTracking()
                .FirstOrDefault();
            var temList = ObjectMapper.Map<MessageInfo, MessageInfoUpdateDto>(query);
            return temList;
        }


        /// <summary>
        /// 添加一个消息类型
        ///</summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [UnitOfWork(false)]
        [HttpPost]
        public async Task<MessageInfoDto> CreateItemAsync(MessageInfoAddDto input)
        {

            var newu = ObjectMapper.Map<MessageInfoAddDto, MessageInfo>(input);
            //添加自定义
            var updated = await _repository.InsertAsync(newu, true);
            var backinfo = ObjectMapper.Map<MessageInfo, MessageInfoDto>(updated);
            return backinfo;
        }
        /// <summary>
        /// 更新一个消息类型
        ///</summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [UnitOfWork(false)]
        [HttpPost]
        public async Task<MessageInfoDto> UpdateItemAsync(MessageInfoUpdateDto input)
        {
            var info = _dbContext.MessageInfo.Where(x => x.Id == input.Id).FirstOrDefault();
            if (info == null || info == default)
            {
                throw new UserFriendlyException("需要查询的信息不存在", "404");
            }
            _appCache.RemoveKey(string.Format(PublicString.CacheItemMessage, info.Code));
            ObjectMapper.Map<MessageInfoUpdateDto, MessageInfo>(input, info);
            await _dbContext.SaveChangesAsync();
            //var updated = await _repository.UpdateAsync(newu);
            var backinfo = ObjectMapper.Map<MessageInfo, MessageInfoDto>(info);
            return backinfo;
        }

        /// <summary>
        /// 删除消息类型软删除
        ///</summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<int> DeleteItemById(int id)
        {
            var aione = await _repository.GetAsync(xy => xy.Id == id);//.FirstOrDefault();
            if (aione != default)
            {
                //await _repository.DeleteAsync(aione);
            }
            return 1;
        }

    }
}
