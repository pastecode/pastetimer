﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Volo.Abp;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Domain.Repositories;
using Volo.Abp.Uow;

namespace PasteTimer.noticemodels
{

    /// <summary>
    /// 接收者
    ///</summary>
    [TypeFilter(typeof(RoleAttribute), Arguments = new object[] { "data", "view" })]
    public class RevicerInfoAppService : PasteTimerAppService
    {

        private readonly IRepository<RevicerInfo, int> _repository;
        private readonly IPasteTimerDbContext _dbContext;
        private readonly IAppCache _appCache;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="repository"></param>
        /// <param name="dbContext"></param>
        /// <param name="appCache"></param>
        public RevicerInfoAppService(
            IRepository<RevicerInfo, int> repository,
            IPasteTimerDbContext dbContext,
            IAppCache appCache)
        {
            _repository = repository;
            _dbContext = dbContext;
            _appCache = appCache;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="page"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PagedResultDto<RevicerInfoListDto>> GetListAsync(int page = 1, int size = 20)
        {

            var query = _dbContext.RevicerInfo.Where(t => 1 == 1).OrderByDescending(xy => xy.Id);
            var pagedResultDto = new PagedResultDto<RevicerInfoListDto>();
            pagedResultDto.TotalCount = await query.CountAsync();
            var userList = await query.Page(page, size).ToListAsync();
            var temList = ObjectMapper.Map<List<RevicerInfo>, List<RevicerInfoListDto>>(userList);
            pagedResultDto.Items = temList;
            return pagedResultDto;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PagedResultDto<RevicerInfoListDto>> Page([FromQuery]InputQueryBase input)
        {
            var query = _dbContext.RevicerInfo.Where(t => 1 == 1).OrderByDescending(xy => xy.Id);
            var _pagedto = new PagedResultDto<RevicerInfoListDto>();
            _pagedto.TotalCount = await query.CountAsync();
            var userList = await query.Page(input.page, input.size).ToListAsync();
            var temList = ObjectMapper.Map<List<RevicerInfo>, List<RevicerInfoListDto>>(userList);
            _pagedto.Items = temList;
            return _pagedto;
        }

        /// <summary>
        /// 根据ID获取待更新单项信息接收者
        ///</summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public RevicerInfoUpdateDto GetInfoForUpdateAsync(int id)
        {
            var query = _dbContext.RevicerInfo.Where(t => t.Id == id)
                .FirstOrDefault();
            var temList = ObjectMapper.Map<RevicerInfo, RevicerInfoUpdateDto>(query);
            return temList;
        }


        /// <summary>
        /// 添加一个接收者
        ///</summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [UnitOfWork(false)]
        [HttpPost]
        public async Task<RevicerInfoDto> CreateItemAsync(RevicerInfoAddDto input)
        {

            var newu = ObjectMapper.Map<RevicerInfoAddDto, RevicerInfo>(input);
            //添加自定义
            var updated = await _repository.InsertAsync(newu, true);
            var backinfo = ObjectMapper.Map<RevicerInfo, RevicerInfoDto>(updated);
            return backinfo;
        }
        /// <summary>
        /// 更新一个接收者
        ///</summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [UnitOfWork(false)]
        [HttpPost]
        public async Task<RevicerInfoDto> UpdateItemAsync(RevicerInfoUpdateDto input)
        {
            var info = _dbContext.RevicerInfo.Where(x => x.Id == input.Id).FirstOrDefault();
            if (info == null || info == default)
            {
                throw new UserFriendlyException("需要查询的信息不存在", "404");
            }
            ObjectMapper.Map<RevicerInfoUpdateDto, RevicerInfo>(input, info);
            await _dbContext.SaveChangesAsync();
            _appCache.RemoveKey(string.Format(PublicString.CacheItemRevicer, input.Id));
            _appCache.RemoveKey(PublicString.CacheListRevicer);
            var backinfo = ObjectMapper.Map<RevicerInfo, RevicerInfoDto>(info);
            return backinfo;
        }

    }
}
