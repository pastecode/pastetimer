﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Domain.Repositories;

namespace PasteTimer.noticemodels
{

    /// <summary>
    /// 发送记录
    ///</summary>
    [TypeFilter(typeof(RoleAttribute), Arguments = new object[] { "data", "view" })]
    public class NoticeLogAppService : PasteTimerAppService
    {

        /// <summary>
        /// 
        /// </summary>
        private readonly IRepository<NoticeLog, int> _repository;

        /// <summary>
        /// 
        /// </summary>
        private readonly IPasteTimerDbContext _dbContext;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="repository"></param>
        /// <param name="dbContext"></param>
        public NoticeLogAppService(
            IRepository<NoticeLog, int> repository,
            IPasteTimerDbContext dbContext
        )
        {
            _repository = repository;
            _dbContext = dbContext;
        }

        /// <summary>
        /// 按页查询记录
        /// </summary>
        /// <param name="page"></param>
        /// <param name="size"></param>
        /// <param name="revicerid"></param>
        /// <returns></returns>
        [HttpGet]
        [Obsolete("use page")]
        public async Task<PagedResultDto<dynamic>> GetListAsync(int page = 1, int size = 20, int revicerid = 0)
        {
            var query = _dbContext.NoticeLog.Where(t => 1 == 1).OrderByDescending(xy => xy.Id);
            var user = _dbContext.RevicerInfo.Where(x => 1 == 1).WhereIf(revicerid != 0, x => x.Id == revicerid);
            var pagequery = from log in query
                            join c in user on log.RevicerId equals c.Id into aaa
                            from revicer in aaa.DefaultIfEmpty()
                            select new
                            {
                                CreateDate = log.CreateDate,
                                Id = log.Id,
                                ObjId = log.ObjId,
                                Body = log.Body,
                                Time = log.Time,
                                Name = revicer.Name,
                                Result = log.Success
                            };

            var pagedResultDto = new PagedResultDto<dynamic>();
            pagedResultDto.TotalCount = await query.CountAsync();
            var userList = await query.Page(page, size).ToListAsync();
            var temList = await pagequery.ToListAsync();
            pagedResultDto.Items = temList;
            return pagedResultDto;
        }

        /// <summary>
        /// 按页查询记录
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PagedResultDto<dynamic>> Page([FromQuery]InputQueryNoticeLog input)
        {
            var query = _dbContext.NoticeLog.Where(t => 1 == 1).OrderByDescending(xy => xy.Id);
            var user = _dbContext.RevicerInfo.Where(x => 1 == 1).WhereIf(input.revicerid != 0, x => x.Id == input.revicerid);
            var pagequery = from log in query
                            join c in user on log.RevicerId equals c.Id into aaa
                            from revicer in aaa.DefaultIfEmpty()
                            select new
                            {
                                CreateDate = log.CreateDate,
                                Id = log.Id,
                                ObjId = log.ObjId,
                                Body = log.Body,
                                Time = log.Time,
                                Name = revicer.Name,
                                Result = log.Success
                            };

            var pagedResultDto = new PagedResultDto<dynamic>();
            pagedResultDto.TotalCount = await query.CountAsync();
            var userList = await query.Page(input.page, input.size).ToListAsync();
            var temList = await pagequery.ToListAsync();
            pagedResultDto.Items = temList;
            return pagedResultDto;
        }

    }
}
