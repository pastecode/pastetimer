﻿//using System;
//using System.Reflection;
//using Microsoft.Extensions.DependencyInjection;

//namespace PasteCodeTaskBase
//{
//    public static class PasteTimerModel
//    {

//        /// <summary>
//        /// 启动服务的各个组件
//        /// 有远端任务的时候调用这个，如果调用了这个就不需要调用AddLocalTaskService了
//        /// 注入实现 示例Services.AddSingleton<IPasteCodeTaskBase, SingleTask>();
//        /// 注意要先注入配置信息，示例Services.Configure<TaskNodeConfig>(abc.GetSection("TaskNodeConfig"));
//        /// AddHttpClient
//        /// TaskChannelHelper
//        /// TaskClientController
//        /// TaskRemoteHostedService
//        /// </summary>
//        /// <param name="services"></param>
//        /// <returns></returns>
//        [Obsolete("Use PasteTaskModule.AddTaskService")]
//        public static void AddTaskService(this IServiceCollection services)
//        {
//            services.AddHttpClient();
//            services.AddSingleton<TaskChannelHelper>();
//            services.AddTransient<TaskClientController>();
//            services.AddHostedService<TaskRemoteHostedService>();
//        }

//        /// <summary>
//        /// 启动服务的各个组件
//        /// 只有本地任务的时候调用这个
//        /// 注入实现 示例Services.AddSingleton<IPasteCodeTaskBase, SingleTask>();
//        /// 注意要先注入配置信息，示例Services.Configure<TaskNodeConfig>(abc.GetSection("TaskNodeConfig"));
//        /// AddHttpClient
//        /// TaskChannelHelper
//        /// TaskClientController
//        /// TaskLocalService
//        /// </summary>
//        /// <param name="services"></param>
//        /// <returns></returns>
//        [Obsolete("Use PasteTaskModule.AddLocalTaskService")]
//        public static void AddLocalTaskService(this IServiceCollection services)
//        {
//            services.AddHttpClient();
//            services.AddSingleton<TaskChannelHelper>();
//            services.AddTransient<TaskClientController>();
//            services.AddHostedService<TaskLocalService>();
//        }

//    }
//}
