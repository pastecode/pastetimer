﻿using System;
using System.Threading.Tasks;

namespace PasteCodeTaskBase
{

    /// <summary>
    /// 
    /// </summary>
    public abstract class IPasteCodeTaskBase : IDisposable
    {


        /// <summary>
        /// 这里可以执行一些初始化操作，比如加载自己的配置文件
        /// </summary>
        /// <param name="context"></param>
        public virtual void Initialize()
        {
            ///TODO:
        }

        /// <summary>
        /// 是否启用本地服务 默认不启用
        /// </summary>
        public virtual bool IsLocationService()
        {
            return false;
        }

        /// <summary>
        /// 定时间隔
        /// </summary>
        private int _second = -1;

        /// <summary>
        /// 定时器正则
        /// 默认 00:00:20 表示每隔20秒执行一次
        /// 每隔(严格8位数) 00:00:00 执行一次 
        /// 正则(.*):05:00表示每个小时的05分钟00秒执行一次
        /// </summary>
        /// <returns></returns>
        public virtual string LocationTickRegex()
        {
            return "00:00:20";
        }

        /// <summary>
        /// 定时时间
        /// </summary>
        /// <returns></returns>
        public int Second()
        {
            if (_second != -1)
            {
                return _second;
            }
            else
            {
                var str = LocationTickRegex();
                if (str.Length == 8)
                {
                    var strs = str.Split(':');
                    if (strs.Length == 3)
                    {
                        int.TryParse(strs[0], out var hour);
                        int.TryParse(strs[1], out var minute);
                        int.TryParse(strs[2], out var second);
                        _second = hour * 3600 + minute * 60 + second;
                    }
                    else
                    {
                        _second = 0;
                    }
                }
                else
                {
                    _second = 0;
                }
            }
            return _second;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public virtual Task<PasteTaskCallBackModel> Work(PasteTaskSharedModel info)
        {
            return Task.Run(() => { return new PasteTaskCallBackModel(); });
        }

        /// <summary>
        /// 停止任务后可能需要的处理
        /// </summary>
        public virtual void Dispose()
        {
            ///TODO:
        }

    }


    public interface IPasteCodeTask
    {

    }

}
