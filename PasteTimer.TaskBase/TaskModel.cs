﻿using System;
using System.ComponentModel;

namespace PasteCodeTaskBase
{
    /// <summary>
    /// 
    /// </summary>
    public class PasteTaskSharedModel
    {
        /// <summary>
        /// 
        /// </summary>
        public int Id { get; set; } = 0;

        ///<summary>
        ///程序集名称
        ///</summary>
        public string Assembly { get; set; } = "";

        ///<summary>
        ///程序集压缩文件地址 如果是内置的则为空  是否可以动态依赖注入？ Project.Service.dll-->project.service.zip -->
        ///</summary>
        public string AssemblyZip { get; set; } = "";
        /// <summary>
        /// 文件版本，上传一次改变一次
        /// </summary>
        public int FileVersion { get; set; }

        /// <summary>
        /// 1：时间点 2:轮询 3:每当 4:队列
        /// </summary>
        public int TaskType { get; set; } = 1;

        /// <summary>
        /// 扩展 任务流水ID
        /// </summary>
        public int TaskLogId { get; set; } = 0;

        /// <summary>
        /// 扩展 任务流水串号
        /// </summary>
        public string TaskLogGuid { get; set; } = "";

        /// <summary>
        /// 执行的节点ID
        /// </summary>
        public int NodeId { get; set; }

        /// <summary>
        /// 扩展 任务时间
        /// </summary>
        public long TaskTime { get; set; }

        /// <summary>
        /// 任务参数
        /// </summary>
        public string HttpBody { get; set; } = "";
    }

    /// <summary>
    /// 
    /// </summary>
    public class PasteTaskCallBackModel
    {

        /// <summary>
        /// 任务ID 外部统一赋值
        /// </summary>
        [Description("任务的ID Task.Id")]
        public int taskid { get; set; }

        /// <summary>
        /// 执行ID 外部统一赋值
        /// </summary>
        [Description("任务记录的ID TaskLog.Id")]
        public int logid { get; set; }

        /// <summary>
        /// 节点ID 外部统一赋值
        /// </summary>
        [Description("节点ID Node.Id")]
        public int nodeid { get; set; }

        /// <summary>
        /// 执行结果 200 500 400
        /// </summary>
        [Description("执行结果 200 500 400")]
        public int code { get; set; } = 200;

        /// <summary>
        /// 返回结果
        /// </summary>
        [Description("结果信息")]
        public string message { get; set; } = "";

        /// <summary>
        /// 花费时长 外部统一赋值
        /// </summary>
        [Description("耗时second")]
        public int duration { get; set; } = 0;
    }

    /// <summary>
    /// 
    /// </summary>
    [Obsolete("Use PasteTaskSharedModel")]
    public class TaskSharedDto
    {
        /// <summary>
        /// 
        /// </summary>
        public int Id { get; set; } = 0;

        ///<summary>
        ///程序集名称
        ///</summary>
        public string Assembly { get; set; } = "";

        ///<summary>
        ///程序集压缩文件地址 如果是内置的则为空  是否可以动态依赖注入？ Project.Service.dll-->project.service.zip -->
        ///</summary>
        public string AssemblyZip { get; set; } = "";
        /// <summary>
        /// 文件版本，上传一次改变一次
        /// </summary>
        public int FileVersion { get; set; }

        /// <summary>
        /// 1：时间点 2:轮询 3:每当 4:队列
        /// </summary>
        public int TaskType { get; set; } = 1;

        /// <summary>
        /// 扩展 任务流水ID
        /// </summary>
        public int TaskLogId { get; set; } = 0;

        /// <summary>
        /// 扩展 任务流水串号
        /// </summary>
        public string TaskLogGuid { get; set; } = "";

        /// <summary>
        /// 执行的节点ID
        /// </summary>
        public int NodeId { get; set; }

        /// <summary>
        /// 扩展 任务时间
        /// </summary>
        public long TaskTime { get; set; }

        /// <summary>
        /// 任务参数
        /// </summary>
        public string HttpBody { get; set; } = "";
    }

    [Obsolete("Use PasteTaskCallBackModel")]
    public class TaskCallBackDto
    {

        /// <summary>
        /// 任务ID 外部统一赋值
        /// </summary>
        [Description("任务的ID Task.Id")]
        public int taskid { get; set; }

        /// <summary>
        /// 执行ID 外部统一赋值
        /// </summary>
        [Description("任务记录的ID TaskLog.Id")]
        public int logid { get; set; }

        /// <summary>
        /// 节点ID 外部统一赋值
        /// </summary>
        [Description("节点ID Node.Id")]
        public int nodeid { get; set; }

        /// <summary>
        /// 执行结果 200 500 400
        /// </summary>
        [Description("执行结果 200 500 400")]
        public int code { get; set; } = 200;

        /// <summary>
        /// 返回结果
        /// </summary>
        [Description("结果信息")]
        public string message { get; set; } = "";

        /// <summary>
        /// 花费时长 外部统一赋值
        /// </summary>
        [Description("耗时second")]
        public int duration { get; set; } = 0;
    }

    /// <summary>
    /// 
    /// </summary>
    public class PasteTaskJoinInfo
    {
        /// <summary>
        /// http://111.111.111.111/
        /// </summary>
        public string Url { get; set; } = "";

        /// <summary>
        /// 
        /// </summary>
        public string JoinToken { get; set; } = "";

        /// <summary>
        /// 
        /// </summary>
        public string Group { get; set; } = "";

        /// <summary>
        /// 客户端代码
        /// </summary>
        public string ClientCode { get; set; } = "123";
    }

    /// <summary>
    /// 用于外部新增
    /// </summary>
    public class PasteTaskAddModel
    {

        ///<summary>
        ///群组名称 允许发送给哪些组的节点执行 *:不挑剔/default:发送给default的组的节点执行/abc,bbc:可以发送给这2个组的节点执行
        ///</summary>
        //[MaxLength(64, ErrorMessage = "群组名称最长不超过64位，表示用这个群组的节点来执行任务！")]
        public string Groups { get; set; } = "default";

        /// <summary>
        /// 任务名称
        /// </summary>
        //[MaxLength(24, ErrorMessage = "任务名称过程，不超过24字!")]
        public string Name { get; set; } = "";

        ///<summary>
        ///程序集名称
        ///</summary>
        //[MaxLength(64)]
        public string Assembly { get; set; } = "";

        ///<summary>
        ///任务类型
        ///</summary>
        //public TaskType TaskType { get; set; } = TaskType.Tick;

        ///<summary>
        ///拉取模式 http模式如何传参?一律采用POST模式 全部body传参？
        ///</summary>
        //public RunWay RunWay { get; set; } = RunWay.Assembly;

        ///<summary>
        ///程序集压缩文件地址 如果是内置的则为空  是否可以动态依赖注入？ Project.Service.dll-->project.service.zip -->
        ///</summary>
        //[MaxLength(256)]
        //public string AssemblyZip { get; set; } = "";

        ///<summary>
        ///任务启动时间
        ///</summary>
        public DateTime StartDate { get; set; } = DateTime.Now;

        ///<summary>
        ///自动结束时间
        ///</summary>
        public DateTime EndDate { get; set; } = DateTime.Now.AddMonths(1);

        ///<summary>
        ///时间正则Wyyyy-MM-dd HH:mm:ss/HH:mm:ss
        ///表示一个点，或者是时间符合这个规则
        ///</summary>
        //[MaxLength(128, ErrorMessage = "规则最长128，示例yyyy-MM-dd HH:mm:ss/HH:mm:ss/Sunyyyy-MM-dd HH:mm:ss")]
        public string TickRegex { get; set; } = "00:00:10";

        ///<summary>
        ///请求路径 示例：api/task/dowork http模式的请求路径
        ///</summary>
        //[MaxLength(128, ErrorMessage = "自定义路径最大128位，请重新输入！")]
        //public string HttpPath { get; set; } = "";

        ///<summary>
        ///Http模式的请求Body
        ///</summary>
        public string HttpBody { get; set; } = "";

        ///// <summary>
        ///// 状态
        ///// </summary>
        //public bool IsEnable { get; set; } = true;

        /// <summary>
        /// 
        /// </summary>
        public int RetryCount { get; set; } = 0;

        /// <summary>
        /// 用于远端新建任务的时候校验，必须要和对应节点node的jointoken一致
        /// </summary>
        public string JoinToken { get; set; } = "";

        /// <summary>
        /// 远端新建任务的时候 用于排重使用的 建议结合jointoken进行加密后获得 32
        /// </summary>
        //[MaxLength(32, ErrorMessage = "唯一码最长32位，请重新输入！")]
        public string MutexCode { get; set; } = "";
    }

    /// <summary>
    /// 用于远端 终止一个任务 或读取一个任务
    /// </summary>
    public class PasteTaskPointModel
    {
        ///<summary>
        ///群组名称 允许发送给哪些组的节点执行 *:不挑剔/default:发送给default的组的节点执行/abc,bbc:可以发送给这2个组的节点执行
        ///</summary>
        //[MaxLength(64, ErrorMessage = "群组名称最长不超过64位，表示用这个群组的节点来执行任务！")]
        public string Groups { get; set; } = "default";

        /// <summary>
        /// 用于远端新建任务的时候校验，必须要和对应节点node的jointoken一致
        /// </summary>
        public string JoinToken { get; set; } = "";

        /// <summary>
        /// 远端新建任务的时候 用于排重使用的 建议结合jointoken进行加密后获得 32
        /// </summary>
        //[MaxLength(32, ErrorMessage = "唯一码最长32位，请重新输入！")]
        public string MutexCode { get; set; } = "";
    }

    /// <summary>
    /// 节点配置信息
    /// </summary>
    public class TaskNodeConfig
    {
        /// <summary>
        /// 本机地址
        /// </summary>
        public string Url { get; set; } = "";
        /// <summary>
        /// 校验密钥
        /// </summary>
        public string JoinToken { get; set; } = "";
        /// <summary>
        /// 组别名称
        /// </summary>
        public string Group { get; set; } = "";

        /// <summary>
        /// 任务调度中心地址 http://111.111.111.111:1234/
        /// </summary>
        public string MasterUrl { get; set; } = "";

        /// <summary>
        /// 最大并发数，同时执行任务数
        /// </summary>
        public int MaxThreadCount { get; set; } = 10;

        /// <summary>
        /// 系统密钥 stoken
        /// </summary>
        public string PublicToken { get; set; } = "";

        /// <summary>
        /// 客户端代码，用于集群的时候互不干扰
        /// </summary>
        public string ClientCode { get; set; } = "default";
    }

    /// <summary>
    /// 
    /// </summary>
    public static class Expland
    {
        /// <summary>
        /// 转化成小写的md5
        /// </summary>
        /// <param name="inputValue"></param>
        /// <returns></returns>
        public static string ToMd5Lower(this string inputValue)
        {

            using (var md5 = System.Security.Cryptography.MD5.Create())
            {
                var result = md5.ComputeHash(System.Text.Encoding.UTF8.GetBytes(inputValue));
                var strResult = BitConverter.ToString(result);
                return strResult.Replace("-", "").ToLower();
            }

        }
    }

    /// <summary>
    /// 添加本地任务
    /// </summary>
    public class PasteTaskOrderModel
    {
        /// <summary>
        /// add添加命令 remove移除命令
        /// </summary>
        public string Action { get; set; } = "add";

        /// <summary>
        /// 唯一码，用于排重
        /// </summary>
        public string SingleCode { get; set; } = "";

        /// <summary>
        /// 程序集名称
        /// </summary>
        public string Assembly { get; set; } = "";

        /// <summary>
        /// 00:00:xx/(.*):05:00
        /// </summary>
        public string TickRegex { get; set; } = "00:00:20";



        /// <summary>
        /// 不需要赋值
        /// </summary>
        public int _Second { get; set; }

        /// <summary>
        /// 不需要赋值 类没找到，等
        /// </summary>
        public int _State { get; set; }

    }

    /// <summary>
    /// 添加本地任务
    /// </summary>
    [Obsolete("Use PasteTaskOrderModel")]
    public class TaskOrderModel
    {
        /// <summary>
        /// add添加命令 remove移除命令
        /// </summary>
        public string Action { get; set; } = "add";

        /// <summary>
        /// 唯一码，用于排重
        /// </summary>
        public string SingleCode { get; set; } = "";

        /// <summary>
        /// 程序集名称
        /// </summary>
        public string Assembly { get; set; } = "";

        /// <summary>
        /// 00:00:xx/(.*):05:00
        /// </summary>
        public string TickRegex { get; set; } = "00:00:20";



        /// <summary>
        /// 不需要赋值
        /// </summary>
        public int _Second { get; set; }

        /// <summary>
        /// 不需要赋值 类没找到，等
        /// </summary>
        public int _State { get; set; }

    }

}
