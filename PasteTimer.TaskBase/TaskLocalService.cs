﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace PasteCodeTaskBase
{
    public class TaskLocalService : IHostedService
    {

        private readonly IServiceProvider _serviceProvider;
        private System.Timers.Timer _timer;
        private readonly ILogger<TaskLocalService> _logger;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="serviceProvider"></param>
        /// <param name="logger"></param>
        public TaskLocalService(
            IServiceProvider serviceProvider,
            ILogger<TaskLocalService> logger)
        {
            _timer = new System.Timers.Timer(1000);
            _serviceProvider = serviceProvider;
            _logger = logger;
            //dictickcount = new System.Collections.Generic.Dictionary<int, int>();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task StartAsync(CancellationToken cancellationToken)
        {
            //Console.WriteLine($"{DateTime.Now} Local Tick HostedService Start!");

            _timer.AutoReset = true;
            _timer.Elapsed += _timer_Elapsed;
            _timer.Start();

            return Task.CompletedTask;
        }
        private long _LastSecond = -1;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {

            var timeoff = DateTimeOffset.Now;
            var _timelong = timeoff.ToUnixTimeSeconds();
            try
            {
                if (_LastSecond == -1)
                {
                    _LastSecond = _timelong - 1;
                }
                //出现了跳秒
                if (_LastSecond + 1 != _timelong && _LastSecond < _timelong)
                {
                    _logger.LogError($"--- --- {DateTime.Now} find jump tick from:{_LastSecond} to:{_timelong} --- ---");
                    using var _scope = _serviceProvider.CreateScope();
                    var tasks = _scope.ServiceProvider.GetServices<IPasteCodeTaskBase>();
                    if (tasks != null)
                    {
                        //如果跳了多个秒呢？
                        for (var k = _LastSecond + 1; k <= _timelong; k++)
                        {
                            var datenow = DateTimeOffset.FromUnixTimeSeconds(k);
                            var datelong = k;
                            var nowstr = datenow.DateTime.ToString("yyyy-MM-dd HH:mm:ss");
                            foreach (var item in tasks)
                            {
                                if (item != null)
                                {
                                    if (item.IsLocationService())
                                    {
                                        var second = item.Second();
                                        if (second > 0)
                                        {
                                            if (datelong % second == 0)
                                            {
                                                item.Work(new PasteTaskSharedModel() { TaskTime = datelong });
                                            }
                                        }
                                        else
                                        {
                                            if (new System.Text.RegularExpressions.Regex(item.LocationTickRegex()).Match(nowstr).Success)
                                            {
                                                //item.TickWorkAsync(datenow, datelong);
                                                item.Work(new PasteTaskSharedModel() { TaskTime = datelong });
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    var nowstr = $"{timeoff.DateTime.DayOfWeek.ToString().Substring(0, 3)} {timeoff.DateTime.ToString("yyyy-MM-dd HH:mm:ss")}";// ;
                    using var _scope = _serviceProvider.CreateScope();
                    var tasks = _scope.ServiceProvider.GetServices<IPasteCodeTaskBase>();
                    if (tasks != null)
                    {
                        foreach (var item in tasks)
                        {
                            if (item != null)
                            {
                                if (item.IsLocationService())
                                {
                                    var second = item.Second();
                                    if (second > 0)
                                    {
                                        if (_timelong % second == 0)
                                        {
                                            //item.TickWorkAsync(timeoff, _timelong);
                                            item.Work(new PasteTaskSharedModel() { TaskTime = _timelong });
                                        }
                                    }
                                    else
                                    {
                                        if (new System.Text.RegularExpressions.Regex(item.LocationTickRegex()).Match(nowstr).Success)
                                        {
                                            //item.TickWorkAsync(timeoff, _timelong);
                                            item.Work(new PasteTaskSharedModel() { TaskTime = _timelong });
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exl)
            {
                _logger.LogError(exl.Message);
            }
            finally
            {
                //重置时间
                _LastSecond = _timelong;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task StopAsync(CancellationToken cancellationToken)
        {
            _timer?.Stop();
            _timer?.Dispose();
            using var _scope = _serviceProvider.CreateScope();
            var tasks = _scope.ServiceProvider.GetServices<IPasteCodeTaskBase>();
            if (tasks != null)
            {
                foreach (var item in tasks)
                {
                    if (item != null)
                    {
                        if (item.IsLocationService())
                        {
                            item.Dispose();
                        }
                    }
                }
            }
            return Task.CompletedTask;
        }

    }
}
