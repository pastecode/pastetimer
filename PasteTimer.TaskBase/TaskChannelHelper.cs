﻿using System.Threading.Channels;

namespace PasteCodeTaskBase
{
    /// <summary>
    /// 需要单例模式注入
    /// </summary>
    public class TaskChannelHelper
    {
        /// <summary>
        /// 
        /// </summary>
        private Channel<PasteTaskSharedModel> _orderqueue;

        private Channel<PasteTaskOrderModel> _localorderqueue;


        /// <summary>
        /// 
        /// </summary>
        public TaskChannelHelper()
        {
            _orderqueue = Channel.CreateUnbounded<PasteTaskSharedModel>();
            _localorderqueue = Channel.CreateUnbounded<PasteTaskOrderModel>();
        }

        /// <summary>
        /// 远程调用，本地请勿调用 
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public async void WriteChannel(PasteTaskSharedModel str)
        {
            await _orderqueue.Writer.WriteAsync(str);

        }

        /// <summary>
        /// 添加命令
        /// </summary>
        /// <param name="job"></param>
        public async void AddOrder(PasteTaskOrderModel job)
        {
            await _localorderqueue.Writer.WriteAsync(job);
        }


        /// <summary>
        /// 远端任务
        /// </summary>
        public Channel<PasteTaskSharedModel> Task { get { return _orderqueue; } }

        /// <summary>
        /// 本地任务
        /// </summary>
        public Channel<PasteTaskOrderModel> Order { get { return _localorderqueue; } }
    }


}
