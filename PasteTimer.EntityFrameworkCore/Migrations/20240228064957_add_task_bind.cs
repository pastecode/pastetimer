﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace PasteTimer.EntityFrameworkCore.Migrations
{
    public partial class add_task_bind : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "TMTaskInfo",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "TMRevicerInfo",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "DataType",
                table: "TMReportInfo",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "TaskId",
                table: "TMReportInfo",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "ClientCodes",
                table: "TMNodeInfo",
                type: "character varying(256)",
                maxLength: 256,
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "TMNodeInfo",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "TMNodeGroupInfo",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(type: "character varying(16)", maxLength: 16, nullable: true),
                    Description = table.Column<string>(type: "character varying(128)", maxLength: 128, nullable: true),
                    Sort = table.Column<int>(type: "integer", nullable: false),
                    UserId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TMNodeGroupInfo", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TMTaskBindUser",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    UserId = table.Column<int>(type: "integer", nullable: false),
                    TaskId = table.Column<int>(type: "integer", nullable: false),
                    Create_UserId = table.Column<int>(type: "integer", nullable: false),
                    CreateDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TMTaskBindUser", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TMNodeGroupInfo_Name",
                table: "TMNodeGroupInfo",
                column: "Name",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TMNodeGroupInfo");

            migrationBuilder.DropTable(
                name: "TMTaskBindUser");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "TMTaskInfo");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "TMRevicerInfo");

            migrationBuilder.DropColumn(
                name: "DataType",
                table: "TMReportInfo");

            migrationBuilder.DropColumn(
                name: "TaskId",
                table: "TMReportInfo");

            migrationBuilder.DropColumn(
                name: "ClientCodes",
                table: "TMNodeInfo");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "TMNodeInfo");
        }
    }
}
