﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PasteTimer.EntityFrameworkCore.Migrations
{
    public partial class add_task_mutexcode : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "MutexCode",
                table: "TMTaskInfo",
                type: "character varying(32)",
                maxLength: 32,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MutexCode",
                table: "TMTaskInfo");
        }
    }
}
