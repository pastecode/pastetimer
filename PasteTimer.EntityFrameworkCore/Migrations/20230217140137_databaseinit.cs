﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace PasteTimer.EntityFrameworkCore.Migrations
{
    public partial class databaseinit : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TMGradeInfo",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(type: "character varying(16)", maxLength: 16, nullable: true),
                    Desc = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    IsEnable = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TMGradeInfo", x => x.Id);
                },
                comment: "角色分组");

            migrationBuilder.CreateTable(
                name: "TMGradeRole",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    GradeId = table.Column<int>(type: "integer", nullable: false),
                    RoleId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TMGradeRole", x => x.Id);
                },
                comment: "角色绑定权限");

            migrationBuilder.CreateTable(
                name: "TMMessageInfo",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Code = table.Column<string>(type: "character varying(16)", maxLength: 16, nullable: true, comment: "消息类型代码"),
                    Desc = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true, comment: "消息介绍"),
                    SendRate = table.Column<int>(type: "integer", nullable: false, comment: "消息发送最小频率")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TMMessageInfo", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TMNodeInfo",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Group = table.Column<string>(type: "character varying(16)", maxLength: 16, nullable: true, comment: "节点分组"),
                    Url = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true, comment: "节点地址"),
                    Status = table.Column<int>(type: "integer", nullable: false, comment: "运行状态"),
                    IsEnable = table.Column<bool>(type: "boolean", nullable: false, comment: "可用"),
                    TotalTask = table.Column<int>(type: "integer", nullable: false, comment: "共执行任务数"),
                    TotalFailed = table.Column<int>(type: "integer", nullable: false, comment: "总失败任务数"),
                    JoinToken = table.Column<string>(type: "character varying(128)", maxLength: 128, nullable: true, comment: "拉取密钥"),
                    Token = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TMNodeInfo", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TMNoticeLog",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CreateDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    Code = table.Column<string>(type: "character varying(16)", maxLength: 16, nullable: true),
                    ObjId = table.Column<int>(type: "integer", nullable: false),
                    Body = table.Column<string>(type: "text", nullable: true),
                    Time = table.Column<int>(type: "integer", nullable: false),
                    RevicerId = table.Column<int>(type: "integer", nullable: false),
                    Success = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TMNoticeLog", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TMReportInfo",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    NodeId = table.Column<int>(type: "integer", nullable: false),
                    Total = table.Column<int>(type: "integer", nullable: false),
                    Success = table.Column<int>(type: "integer", nullable: false),
                    Failed = table.Column<int>(type: "integer", nullable: false),
                    DataDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TMReportInfo", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TMRevicerInfo",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(type: "character varying(16)", maxLength: 16, nullable: true, comment: "接收者名称"),
                    IsEnable = table.Column<bool>(type: "boolean", nullable: false),
                    Url = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true, comment: "推送地址"),
                    Codes = table.Column<string>(type: "text", nullable: true, comment: "接收消息类型")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TMRevicerInfo", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TMRoleInfo",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Model = table.Column<string>(type: "character varying(16)", maxLength: 16, nullable: true),
                    Name = table.Column<string>(type: "character varying(16)", maxLength: 16, nullable: true),
                    Desc = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    IsEnable = table.Column<bool>(type: "boolean", nullable: false),
                    Sort = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TMRoleInfo", x => x.Id);
                },
                comment: "权限列表");

            migrationBuilder.CreateTable(
                name: "TMTaskInfo",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Groups = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true, comment: "群组名称"),
                    Name = table.Column<string>(type: "character varying(24)", maxLength: 24, nullable: true, comment: "任务名称"),
                    Assembly = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true, comment: "程序集名称"),
                    TaskType = table.Column<int>(type: "integer", nullable: false, comment: "任务类型"),
                    RunWay = table.Column<int>(type: "integer", nullable: false, comment: "拉取模式"),
                    AssemblyZip = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true, comment: "程序集压缩文件地址"),
                    FileVersion = table.Column<int>(type: "integer", nullable: false, comment: "上传一次改变一次"),
                    StartDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false, comment: "任务启动时间"),
                    EndDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false, comment: "自动结束时间"),
                    TickRegex = table.Column<string>(type: "character varying(128)", maxLength: 128, nullable: true, comment: "时间正则"),
                    TickSecond = table.Column<int>(type: "integer", nullable: false, comment: "内部计时秒数"),
                    HttpPath = table.Column<string>(type: "character varying(128)", maxLength: 128, nullable: true, comment: "请求路径"),
                    HttpBody = table.Column<string>(type: "text", nullable: true, comment: "任务参数"),
                    IsEnable = table.Column<bool>(type: "boolean", nullable: false),
                    RetryCount = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TMTaskInfo", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TMTaskLog",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    TaskId = table.Column<int>(type: "integer", nullable: false),
                    CreateDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    NodeId = table.Column<int>(type: "integer", nullable: false),
                    Status = table.Column<int>(type: "integer", nullable: false),
                    Durtion = table.Column<int>(type: "integer", nullable: false),
                    Message = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TMTaskLog", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TMUserInfo",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    UserName = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    CreateDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    Desc = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    Email = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    PassWord = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    IsEnable = table.Column<bool>(type: "boolean", nullable: false),
                    Grade = table.Column<string>(type: "character varying(16)", maxLength: 16, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TMUserInfo", x => x.Id);
                },
                comment: "用户信息");

            migrationBuilder.CreateIndex(
                name: "IX_TMGradeInfo_Name",
                table: "TMGradeInfo",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_TMMessageInfo_Code",
                table: "TMMessageInfo",
                column: "Code",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TMGradeInfo");

            migrationBuilder.DropTable(
                name: "TMGradeRole");

            migrationBuilder.DropTable(
                name: "TMMessageInfo");

            migrationBuilder.DropTable(
                name: "TMNodeInfo");

            migrationBuilder.DropTable(
                name: "TMNoticeLog");

            migrationBuilder.DropTable(
                name: "TMReportInfo");

            migrationBuilder.DropTable(
                name: "TMRevicerInfo");

            migrationBuilder.DropTable(
                name: "TMRoleInfo");

            migrationBuilder.DropTable(
                name: "TMTaskInfo");

            migrationBuilder.DropTable(
                name: "TMTaskLog");

            migrationBuilder.DropTable(
                name: "TMUserInfo");
        }
    }
}
