﻿using System;
using Microsoft.EntityFrameworkCore;
using PasteTimer.noticemodels;
using PasteTimer.taskmodels;
using PasteTimer.usermodels;
using Volo.Abp;
using Volo.Abp.EntityFrameworkCore.Modeling;

namespace PasteTimer
{
    public static class PasteTimerDbContextModelCreatingExtensions
    {
        public static void ConfigurePasteTimer(
            this ModelBuilder builder,
            Action<PasteTimerModelBuilderConfigurationOptions> optionsAction = null)
        {


            Check.NotNull(builder, nameof(builder));

            var options = new PasteTimerModelBuilderConfigurationOptions(
                PasteTimerDbProperties.DbTablePrefix,
                PasteTimerDbProperties.DbSchema
            );

            optionsAction?.Invoke(options);

            /* Configure all entities here. Example:

            builder.Entity<Question>(b =>
            {
                //Configure table & schema name
                b.ToTable(options.TablePrefix + "Questions", options.Schema);
            
                b.ConfigureByConvention();
            
                //Properties
                b.Property(q => q.Title).IsRequired().HasMaxLength(QuestionConsts.MaxTitleLength);
                
                //Relations
                b.HasMany(question => question.Tags).WithOne().HasForeignKey(qt => qt.QuestionId);

                //Indexes
                b.HasIndex(q => q.CreationTime);
            });
            */


            // *NodeInfo*
            builder.Entity<NodeInfo>(b =>
            {
                b.ToTable(options.TablePrefix + "NodeInfo", options.Schema);
                b.ConfigureByConvention();
            });

            // *TaskInfo*
            builder.Entity<TaskInfo>(b =>
            {
                b.ToTable(options.TablePrefix + "TaskInfo", options.Schema);
                b.ConfigureByConvention();
            });

            // *TaskLog*
            builder.Entity<TaskLog>(b =>
            {
                b.ToTable(options.TablePrefix + "TaskLog", options.Schema);
                b.ConfigureByConvention();
            });

            // *ReportInfo*
            builder.Entity<ReportInfo>(b =>
            {
                b.ToTable(options.TablePrefix + "ReportInfo", options.Schema);
                b.ConfigureByConvention();
            });

            // *UserInfo*
            builder.Entity<UserInfo>(b =>
            {
                b.ToTable(options.TablePrefix + "UserInfo", options.Schema);
                b.ConfigureByConvention();
            });

            // *RoleInfo*
            builder.Entity<RoleInfo>(b =>
            {
                b.ToTable(options.TablePrefix + "RoleInfo", options.Schema);
                b.ConfigureByConvention();
            });

            // *GradeInfo*
            builder.Entity<GradeInfo>(b =>
            {
                b.ToTable(options.TablePrefix + "GradeInfo", options.Schema);
                b.ConfigureByConvention();
            });

            // *GradeRole*
            builder.Entity<GradeRole>(b =>
            {
                b.ToTable(options.TablePrefix + "GradeRole", options.Schema);
                b.ConfigureByConvention();
            });

            // *RevicerInfo*
            builder.Entity<RevicerInfo>(b =>
            {
                b.ToTable(options.TablePrefix + "RevicerInfo", options.Schema);
                b.ConfigureByConvention();
            });

            // *MessageInfo*
            builder.Entity<MessageInfo>(b =>
            {
                b.ToTable(options.TablePrefix + "MessageInfo", options.Schema);
                b.ConfigureByConvention();
            });

            // *NoticeLog*
            builder.Entity<NoticeLog>(b =>
            {
                b.ToTable(options.TablePrefix + "NoticeLog", options.Schema);
                b.ConfigureByConvention();
            });

            //**TaskBindUser**
            builder.Entity<TaskBindUser>(b =>
            {
                b.ToTable(options.TablePrefix + "TaskBindUser", options.Schema);
                b.ConfigureByConvention();
            });

            //**NodeGroupInfo**
            builder.Entity<NodeGroupInfo>(b =>
            {
                b.ToTable(options.TablePrefix + "NodeGroupInfo", options.Schema);
                b.ConfigureByConvention();
            });

        }
    }
}