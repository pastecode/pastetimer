﻿using System;
using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace PasteTimer
{
    /// <summary>
    /// Postgresql
    /// </summary>
    public class PgsqlDbContextFactory : IDesignTimeDbContextFactory<PgsqlDbContext>
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public PgsqlDbContext CreateDbContext(string[] args)
        {
            var configuration = BuildConfiguration();

            //启用时区 true:without time zone  false with time zone
            AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);

            var builder = new DbContextOptionsBuilder<PgsqlDbContext>().UseNpgsql(configuration.GetConnectionString(PasteTimerDbProperties.ConnectionStringName));

            return new PgsqlDbContext(builder.Options);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private static IConfigurationRoot BuildConfiguration()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false);

            return builder.Build();
        }
    }




}
