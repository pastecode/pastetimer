﻿using System;
using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace PasteTimer
{
    /// <summary>
    /// Sqlite
    /// </summary>
    public class SqliteDbContextFactory : IDesignTimeDbContextFactory<SqliteDbContext>
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public SqliteDbContext CreateDbContext(string[] args)
        {
            var configuration = BuildConfiguration();

            //启用时区 true:without time zone  false with time zone
            //AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);

            var builder = new DbContextOptionsBuilder<SqliteDbContext>().UseSqlite(configuration.GetConnectionString(PasteTimerDbProperties.SqliteConnectionStringName));

            return new SqliteDbContext(builder.Options);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private static IConfigurationRoot BuildConfiguration()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false);

            return builder.Build();
        }
    }
}
