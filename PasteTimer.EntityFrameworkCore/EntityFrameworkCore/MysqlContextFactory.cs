﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace PasteTimer
{
    /// <summary>
    /// MySql
    /// </summary>
    public class MysqlContextFactory : IDesignTimeDbContextFactory<MysqlDbContext>
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public MysqlDbContext CreateDbContext(string[] args)
        {
            var configuration = BuildConfiguration();

            //启用时区 true:without time zone  false with time zone
            //AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);

            var builder = new DbContextOptionsBuilder<MysqlDbContext>().UseMySql(configuration.GetConnectionString(PasteTimerDbProperties.MysqlConnectionStringName), ServerVersion.AutoDetect(configuration.GetConnectionString(PasteTimerDbProperties.MysqlConnectionStringName)));

            return new MysqlDbContext(builder.Options);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private static IConfigurationRoot BuildConfiguration()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false);

            return builder.Build();
        }
    }
}
