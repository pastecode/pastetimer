﻿using System;
using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace PasteTimer
{

    /// <summary>
    /// Postgresql
    /// </summary>
    public class SqlserverDbContextFactory : IDesignTimeDbContextFactory<SqlserverDbContext>
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public SqlserverDbContext CreateDbContext(string[] args)
        {
            var configuration = BuildConfiguration();

            //启用时区 true:without time zone  false with time zone
            //AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);

            var builder = new DbContextOptionsBuilder<SqlserverDbContext>().UseSqlServer(configuration.GetConnectionString(PasteTimerDbProperties.SqlserverConnectionStringName));

            return new SqlserverDbContext(builder.Options);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private static IConfigurationRoot BuildConfiguration()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false);

            return builder.Build();
        }
    }
}
