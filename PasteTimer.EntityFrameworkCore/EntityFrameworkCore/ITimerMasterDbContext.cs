﻿using Microsoft.EntityFrameworkCore;
using PasteTimer.noticemodels;
using PasteTimer.taskmodels;
using PasteTimer.usermodels;
using Volo.Abp.EntityFrameworkCore;

namespace PasteTimer
{
    /// <summary>
    /// 
    /// </summary>
    //[ConnectionStringName(PasteTimerDbProperties.ConnectionStringName)]
    public interface IPasteTimerDbContext : IEfCoreDbContext
    {
        /* Add DbSet for each Aggregate Root here. Example:
         * DbSet<Question> Questions { get; }
         */
        /// <summary>
        /// 
        /// </summary>
        public DbSet<NodeInfo> NodeInfo { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DbSet<TaskInfo> TaskInfo { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DbSet<TaskLog> TaskLog { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DbSet<ReportInfo> ReportInfo { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DbSet<UserInfo> UserInfo { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DbSet<RoleInfo> RoleInfo { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DbSet<GradeInfo> GradeInfo { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DbSet<GradeRole> GradeRole { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DbSet<RevicerInfo> RevicerInfo { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DbSet<MessageInfo> MessageInfo { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DbSet<NoticeLog> NoticeLog { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DbSet<TaskBindUser> TaskBindUser { get; set; }

        /// <summary>
        /// 节点分组信息
        /// </summary>
        public DbSet<NodeGroupInfo> NodeGroupInfo { get; set; }

    }
}