﻿using Microsoft.Extensions.DependencyInjection;
using System;
using Volo.Abp.EntityFrameworkCore;
using Volo.Abp.Modularity;


namespace PasteTimer
{
    /// <summary>
    /// 
    /// </summary>
    [DependsOn(
        typeof(PasteTimerDomainModule),
        typeof(AbpEntityFrameworkCoreModule)
    )]
    public class PasteTimerEntityFrameworkCoreModule : AbpModule
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            var config = context.Services.GetConfiguration();
            var datatype = config.GetSection("TaskConfig:DataType").Value;
            if (!String.IsNullOrEmpty(datatype))
            {
                if (datatype == "postgresql")
                {
                    context.Services.AddTransient<IPasteTimerDbContext,PgsqlDbContext>();

                    //Console.WriteLine("---------PasteTimerEntityFrameworkCoreModule ConfigureServices ---------");
                    AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);
                    //AppContext.SetSwitch("Npgsql.DisableDateTimeInfinityConversions", true);
                    context.Services.AddAbpDbContext<PgsqlDbContext>(options =>
                    {
                        Configure<AbpDbContextOptions>(aa => { aa.UseNpgsql(); });
                        options.AddDefaultRepositories<PgsqlDbContext>(includeAllEntities: true);
                    });
                }
            }
        }
    }
}