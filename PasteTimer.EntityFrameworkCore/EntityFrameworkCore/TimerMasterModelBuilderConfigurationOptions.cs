﻿using JetBrains.Annotations;
using Volo.Abp.EntityFrameworkCore.Modeling;

namespace PasteTimer
{
    /// <summary>
    /// 
    /// </summary>
    public class PasteTimerModelBuilderConfigurationOptions : AbpModelBuilderConfigurationOptions
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tablePrefix"></param>
        /// <param name="schema"></param>
        public PasteTimerModelBuilderConfigurationOptions(
            [NotNull] string tablePrefix = "",
            [CanBeNull] string schema = null)
            : base(
                tablePrefix,
                schema)
        {

        }
    }
}