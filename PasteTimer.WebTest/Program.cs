using PasteCodeTaskBase;
using PasteTimer.WebTest;
using Serilog;

var builder = WebApplication.CreateBuilder(args);

var abc = new ConfigurationBuilder()
       .SetBasePath(System.IO.Directory.GetCurrentDirectory())
       .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
       .AddEnvironmentVariables()
       .Build();

Log.Logger = new LoggerConfiguration()
    //.MinimumLevel.Warning()
    //.MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
    .ReadFrom.Configuration(abc)
    //.WriteTo.Console()
    //.WriteTo.File(Path.Combine("Logs", @"log.txt"), rollingInterval: RollingInterval.Day, retainedFileCountLimit: 60, fileSizeLimitBytes: 3145728, rollOnFileSizeLimit: true)
    .Enrich.FromLogContext()
    .CreateLogger();

builder.Configuration.AddConfiguration(abc);

builder.Services.AddSingleton<IPasteCodeTaskBase, SingleOrderTask>();
builder.Services.AddSingleton<IPasteCodeTaskBase, SingleTask>();
builder.Services.AddTransient<IPasteCodeTaskBase, CollceryTask>();
builder.Services.Configure<TaskNodeConfig>(abc.GetSection("TaskNodeConfig"));
builder.Services.AddTaskService();
builder.Services.AddSingleton<IPasteCodeTaskBase, LocalTask>();
builder.Services.AddHostedService<TaskLocalService>();

builder.Services.AddControllers();
builder.Host.UseSerilog();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error");
}
app.UseStaticFiles();
app.UseHttpsRedirection();
app.UseCookiePolicy();
app.UseRouting();

app.UseEndpoints(endpoints =>
{
    endpoints.MapControllers();
});
app.Run();


