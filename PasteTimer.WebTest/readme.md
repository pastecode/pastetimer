﻿
## 任务调度系统的客户端，需要引入需要任务的项目中，然后

```
//配置项目
builder.Services.Configure<TaskNodeConfig>(abc.GetSection("TaskNodeConfig"));
//启用http
builder.Services.AddHttpClient();
//注入内队列帮助类
builder.Services.AddSingleton<TaskChannelHelper>();
//注入公共接口
builder.Services.AddTransient<TaskController>();

//注入自己实现的任务
//builder.Services.AddSingleton<ITaskBase, PasteTimer.WebTest.ReportTask>();
//builder.Services.AddTransient<ITaskBase, PasteTimer.WebTest.CollceryTask>();
//注入服务
builder.Services.AddHostedService<TaskHostedService>();

```

## TaskNodeConfig

```
  "TaskNodeConfig": {
    "Url": "http://192.168.2.50:36001/",//服务端可以访问的当前节点的地址
    "JoinToken": "1122334455",//和服务端交互的安全密钥
    "Group": "default",//当前节点的组名 8位内
    "MasterUrl": "http://192.168.2.50:36000/"//主服务的地址
  },

```

## 回调处理

```
有错误可以直接抛出，如果不抛出错误，请try catch后返回 code 500
```


## 改版成单机或者集群模式！


## MiddleHandler
    1.httpclient
    2.masterurl
    3.apitoken
    4.dataformat model json
    5.redis --> datafrom defaultbase prefixkey


## callback 丢失的可能性
    1.
