﻿using PasteCodeTaskBase;

namespace PasteTimer.WebTest
{

    /// <summary>
    /// 远程服务 同时只能有一个在运行，其他的排队等待执行
    /// </summary>
    public class SingleOrderTask : IPasteCodeTaskBase
    {

        private SemaphoreSlim semaphoreSlim;

        public SingleOrderTask()
        {
            //用注入带参模式
            semaphoreSlim = new SemaphoreSlim(1);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public override async Task<PasteTaskCallBackModel> Work(PasteTaskSharedModel info)
        {
            try
            {
                //模拟单例执行?
                await semaphoreSlim.WaitAsync();

                //这里执行任务代码，注意要返回
                //Console.WriteLine($"{DateTime.Now} do work ReportTask");
                var rand = new Random();
                await Task.Delay(rand.Next(100, 3000));

                var randnum = rand.Next(1, 10);

                return new PasteTaskCallBackModel() { code = 200, logid = info.TaskLogId, message = "work!", taskid = info.Id };

            }
            finally
            {
                semaphoreSlim.Release();
            }
        }

    }


    /// <summary>
    /// 远程服务 同时只能有一个在运行 多余的抛弃掉
    /// </summary>
    public class SingleTask : IPasteCodeTaskBase
    {


        private bool isRunning = false;
        public SingleTask()
        {
        }

        public override async Task<PasteTaskCallBackModel> Work(PasteTaskSharedModel info)
        {
            if (!isRunning)
            {
                isRunning = true;
                try
                {
                    //这里执行任务代码，注意要返回
                    //Console.WriteLine($"{DateTime.Now} do work ReportTask");
                    var rand = new Random();
                    await Task.Delay(rand.Next(100, 3000));

                    var randnum = rand.Next(1, 10);

                    return new PasteTaskCallBackModel() { code = 200, logid = info.TaskLogId, message = "work done!", taskid = info.Id };

                }
                catch (Exception exl)
                {
                    return new PasteTaskCallBackModel() { code = 500, logid = info.TaskLogId, message = exl.Message, taskid = info.Id };
                }
                finally
                {
                    isRunning = false;
                }
            }
            else
            {
                return new PasteTaskCallBackModel() { code = 200, logid = info.TaskLogId, message = "work is running!", taskid = info.Id };
            }
        }

    }

    /// <summary>
    /// 远程服务 回收服务
    /// </summary>
    public class CollceryTask : IPasteCodeTaskBase
    {

        public CollceryTask()
        {
            //这里可以依赖注入
        }

        public override async Task<PasteTaskCallBackModel> Work(PasteTaskSharedModel info)
        {
            //在这里执行任务的内容

            //Console.WriteLine($"{DateTime.Now} do work CollceryTask");
            var rand = new Random();
            await Task.Delay(rand.Next(100, 10000));

            var randnum = rand.Next(1, 10);

            return new PasteTaskCallBackModel() { code = 200, logid = info.TaskLogId, message = "work!", taskid = info.Id };
        }

    }


    /// <summary>
    /// 本地服务 案例
    /// </summary>
    public class LocalTask : IPasteCodeTaskBase
    {

        public LocalTask()
        {
            //这里可以依赖注入
        }

        public override bool IsLocationService()
        {
            return true;
        }

        public override string LocationTickRegex()
        {
            return "01:00:00";
        }

        public override Task<PasteTaskCallBackModel> Work(PasteTaskSharedModel info)
        {
            return base.Work(info);
        }
    }

}
